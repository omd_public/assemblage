---
title: Assemblage Projet - Concept Validation
lang: en
---

# Overview

Assemblage is a composable data browser aimed to provide a great user experience for Solid POD services,
as well as other RDF sources, both for desktop and mobile web browsers.

The frontend uses a registry of remote web components that can be composed into views and applications in accordance to
conditions over the underlying RDF data. Each view or application could have a different output depending on access control
policies, ownership and roles, e.g. owner, reader, writer &c.

The main javascript bundle can be served from the POD service itself, like mashlib, or deployed
in a separate HTTP server as an static resource that will proxy requests based on the URL path.

## Technology Readiness Level

Assemblage is at the _concept validation_ level, altough the implementation is 100% functional.

# Views

Each view provides a presentation for a particular RDF type or structure as remote web component.
Views are responsible to adapt to the current access level. Views can be easly switched in the data browser UI and are activated if the conditions of registration are fulfilled. The conditions are not limited to matching predicates against the current graph and may include roles and audience.

Assemblage currently implements the following views.

## Account

Displays the cover summary of a Solid POD, wich includes:

* The personal profile document
* Solid public and private types
* List of friends

## Profile

Displays a personal profile document, includes: photo/s, name, social links and contact information.

## Preferences

Displays and allows the user to edit:

* Personal and contact information
* Account preferences (from preferences.ttl)
* Access control of external applications

## Settings

Displays the resource access control list and allows to edit it.
Note that uses ACL resource location discovery.

## Storage

This view allows a user to navigate through the RDF resources in a POD.
If the user have write permissions can create new resources and edit existing ones, including LDP containers.

Note that storage also contributes an application, and can be integrated with other applications to view or edit resources,
similar to Google Drive.

# Applications

Applications provide views, launcher entries and, optionally, contributions to other applications (e.g. the available resources to create from the storage app).

## Notepad

This application handles Notepad structures from the Personal Information Management ontology in
a collaborative editing fashion.

## Chat

The chat application is just a placeholder at this stage.

# Project Next Stage

This is a summary of the actions items for the next stage of Assemblage data browser.

The goal of this stage is to deliver a production ready data browser and SDK, mobile friendly, with a great user and developer experience.

* Determine scope
  * Consider implementing a browser extension
  * Internationalization
* Project governance and financials
  * Position regarding overlap with mashlib
* Technical analysis
  * Review key abstractions (emphasis on extensibility)
  * Embrace Shapes, Forms and Footprints[^1]
  * Follow Solid spec, if/when ready
  * Architectural baseline
  * Determine which projects to depend to (e.g. rdflib, acl-check)
  * Framework assesment[^2]
* UX refinement (emphasis on micro-interactions)
* Detailed roadmap of the project

[^1]: See [Linked Data Shapes, Forms and Footprints](https://www.w3.org/DesignIssues/Footprints.html)
[^2]: Gatsby, Next.js, remote loaders &c.

## Clarification about Mashlib

We do not believe that exist a conflict of interests or duplication between Mashlib and Assemblage, they serve to different purposes: (1) Mashlib data browser as the testbed for the evolution of the Solid specifications and (2) Assemblage as a reference implementation of the stablished specifications with focus on user experience and adoption.

# Quality

For the production stage we propose the SQALE method, with a target of 'A' rating.
Sonar Qube could be used to track the automated SQALE indices and to audit the contributions to the source repositories.
Additionally, an inventory of technical debt should be maintained manually.
