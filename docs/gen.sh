#!/usr/bin/env bash

pandoc -N --pdf-engine=xelatex -V linkcolor=blue -V fontsize=12pt -V geometry:a4paper -V geometry=margin=2cm -V mainfont="DejaVu Sans" -V monofont="DejaVu Sans Mono" assemblage_doc.md -o assemblage_doc.pdf