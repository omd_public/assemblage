import React from 'react'
import {
  Box,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from '@material-ui/core'
import { PersonRounded, ToggleOnRounded } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import { SessionContext } from '@assemblage/components'
import { useReify } from '@assemblage/hooks'
import { profileInfo } from '@assemblage/ld'

import ProfileOrganiser from './profile-organiser'
import PersonalInfo from './personal-info/main'
import AppControl from './app-control/main'

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    borderRadius: theme.spacing(1),
    '&$buttonSelected, &$buttonSelected:focus': {
      backgroundColor: theme.palette.grey[200]
    },
    '&$buttonSelected:hover': {
      backgroundColor: theme.palette.grey[300]
    },
    '&:hover': {
      backgroundColor: theme.palette.grey[300]
    }
  },
  buttonSelected: {}
}))

function PrefContent ({ value, data, ...props }) {
  switch (value) {
    case 'p-info':
      return <PersonalInfo data={data.personal} {...props} />
    case 'app-control':
      return <AppControl data={data.appControl} {...props} />
    default:
      return null
  }
}

export default function ({ subjectUrl, ...props }) {
  const classes = useStyles()
  const [value, setValue] = React.useState('p-info')
  const [editorState, setEditorState] = React.useState({
    open: false
  })
  const [profileState, setProfileState] = React.useState(null)
  const { session } = React.useContext(SessionContext)

  if (!session.currentSession) {
    return <Typography>No session, please log in</Typography>
  }

  const webId = session.currentSession.webId

  const state = useReify({
    pro: profileInfo
  }, {
    subjectUrl: webId
  })

  if (state.loading) {
    return null
  }

  if (state.error) {
    return null
  }

  const entity = state.it
  const entityStore = entity.store
  const entityUpdater = entity.updater

  if (profileState === null) {
    setProfileState(entity.pro)
    return null
  }

  const readProfile = () => {
    const profile = profileInfo({ store: entityStore, subjectUrl: webId })
    setProfileState(profile)
  }

  const preferences = {
    userType: session.account.userType,
    mBox: session.account.mBox
  }
  const organised = new ProfileOrganiser(profileState, preferences, entityStore).organise()

  function handleListItemClick (event, item) {
    setValue(item)
  }

  function handleFieldSelection (event, data, key, context, EditComponent) {
    setEditorState({
      open: true,
      data,
      key,
      context,
      EditComponent
    })
  }

  const handleEditPanelClose = () => {
    setEditorState({
      open: false
    })
  }

  return (
    <div {...props}>
      <Box display='flex' flexWrap='wrap'>
        <Box m={1} p={2}>
          <List>
            <ListItem
              button
              classes={{
                root: classes.button,
                selected: classes.buttonSelected
              }}
              selected={value === 'p-info'}
              onClick={event => handleListItemClick(event, 'p-info')}
            >
              <ListItemIcon>
                <PersonRounded />
              </ListItemIcon>
              <ListItemText primary='Personal Info' />
            </ListItem>
            <ListItem
              button
              classes={{
                root: classes.button,
                selected: classes.buttonSelected
              }}
              selected={value === 'app-control'}
              onClick={event => handleListItemClick(event, 'app-control')}
            >
              <ListItemIcon>
                <ToggleOnRounded />
              </ListItemIcon>
              <ListItemText primary='App Control' />
            </ListItem>
          </List>
        </Box>
        <Box flexGrow={1} m={1} p={2}>
          <PrefContent
            value={value}
            data={organised}
            clickHandler={handleFieldSelection}
            {...props}
          />
        </Box>
      </Box>
      {editorState.EditComponent &&
        <editorState.EditComponent
          editorState={editorState}
          handleClose={handleEditPanelClose}
          refreshComponent={readProfile}
          profileStore={entityStore}
          profileUpdater={entityUpdater}
          {...props}
        />}
    </div>
  )
}
