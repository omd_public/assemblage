import React from 'react'
import {
  Box,
  Divider,
  Typography
} from '@material-ui/core'
import { InfoBox, ListItemBox } from '../info-components'

function PreferencesPanel ({ ...props }) {
  const { data } = props
  return (
    <InfoBox title='General Preferences'>
      <ListItemBox title='POD EMAIL' dataKey='mbox' infoSet='preference' titleMinWidth={128} {...props}>
        <Box display='flex' flexDirection='column'>
          <Typography>{data.mBox}</Typography>
          <Typography variant='caption' color='textSecondary'>The email address linked to your pod.</Typography>
        </Box>
      </ListItemBox>
      <Divider variant='middle' />
      <ListItemBox title='USER TYPE' dataKey='userType' infoSet='preference' titleMinWidth={128} {...props}>
        <Box display='flex' flexDirection='column'>
          <Typography>{data.userType}</Typography>
          <Typography variant='caption' color='textSecondary'>Your user type defines which views and functionalities you will have access to.</Typography>
        </Box>
      </ListItemBox>
    </InfoBox>
  )
}

export default PreferencesPanel
