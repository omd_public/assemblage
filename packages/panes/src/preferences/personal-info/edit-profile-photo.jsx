import React from 'react'
import { DropzoneArea } from 'material-ui-dropzone'
import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  IconButton,
  Typography
} from '@material-ui/core'
import { DeleteRounded, Close as CloseIcon } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import { SessionContext } from '@assemblage/components'
import LoadingWheel from '../loading-wheel'
import { uploadFile, deleteFile } from './rdf-utils'

const useStyles = makeStyles(theme => ({
  dialogPaper: {
    width: '600px'
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1)
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  },
  delete: {
    color: theme.palette.error.main
  },
  subtitle: {
    'font-size': '1rem',
    'font-weight': '500'
  },
  dropzoneTitle: {
    'font-size': '1rem',
    'margin-left': theme.spacing(1),
    'margin-right': theme.spacing(1)
  },
  error: {
    color: theme.palette.error.main
  }
}))

function EditProfilePhoto ({ ...props }) {
  const classes = useStyles()
  const { editorState, handleClose, refreshComponent, profileStore, profileUpdater } = props
  const { open, data, key } = editorState

  const [newImage, setNewImage] = React.useState(null)
  const [deleteImage, setDeleteImage] = React.useState(null)
  const [updateState, setUpdateState] = React.useState({
    loading: false,
    error: false
  })
  const [files, setFiles] = React.useState([])

  const { session } = React.useContext(SessionContext)
  const webId = session.currentSession.webId
  const profileImages = [].concat(data[key])

  React.useEffect(() => {
    function handleComplete () {
      setUpdateState({ loading: false, error: false })
      handleClose()
      refreshComponent()
    }

    async function uploadImage () {
      setUpdateState({ loading: true })
      try {
        const file = newImage[0]
        const reader = new FileReader()
        reader.onload = async event => {
          var data = event.target.result
          await uploadFile(profileStore, webId, file, data)
          handleComplete()
        }
        reader.readAsArrayBuffer(file)
      } catch (error) {
        setUpdateState({ error: true, errorMessage: error })
      }
    }

    async function performDelete () {
      setUpdateState({ loading: true })
      try {
        await deleteFile(profileStore, profileUpdater, webId, 'vcard:Photo', deleteImage)
        handleComplete()
      } catch (error) {
        setUpdateState({ error: true, errorMessage: error })
      }
    }

    if (newImage && newImage.length > 0) {
      uploadImage()
    } else if (deleteImage) {
      performDelete()
    }
  }, [newImage, deleteImage])

  function handleChange (files) {
    setFiles(files)
  }

  function handleUpload () {
    setNewImage(files)
  }

  function handleDelete (image) {
    setDeleteImage(image)
  }

  if (updateState.loading) {
    return <LoadingWheel {...props} />
  }

  return (
    <Dialog PaperProps={{ classes: { root: classes.dialogPaper } }} open={open} onClose={handleClose}>
      <DialogTitle>Edit profile photo</DialogTitle>
      <IconButton className={classes.closeButton} onClick={handleClose}>
        <CloseIcon />
      </IconButton>
      <Divider />
      <DialogContent>
        <Box display='flex' flexDirection='column'>
          <Box pt={1} pb={1}>
            <Typography className={classes.subtitle}>Current uploaded photos</Typography>
            {profileImages.map((img, i) => {
              return (
                <div key={`edit_img_${i}`}>
                  <Box display='flex' flexDirection='row' alignItems='center' pt={1}>
                    <Box display='flex' flexGrow={1} pl={1}>
                      <Avatar className={classes.avatar} alt={data.name} src={img} />
                    </Box>
                    <Box display='flex' pr={1}>
                      <IconButton className={classes.delete} onClick={event => handleDelete(img)}>
                        <DeleteRounded />
                      </IconButton>
                    </Box>
                  </Box>
                </div>
              )
            })}
          </Box>
          <Box pt={1} pb={1}>
            <Typography className={classes.subtitle}>Add new profile photos</Typography>
            <Box p={1}>
              <DropzoneArea
                dropzoneParagraphClass={classes.dropzoneTitle}
                onChange={handleChange}
                acceptedFiles={['image/*']}
                filesLimit={1}
                showPreviews
                showPreviewsInDropzone={false}
                maxFileSize={1000000}
                onClose={handleClose}
              />
            </Box>
          </Box>
        </Box>
        {updateState.error &&
          <Typography className={classes.error} variant='caption'>
            {updateState.errorMessage}
          </Typography>}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleUpload} color='secondary'>
        Upload
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default EditProfilePhoto
