import React from 'react'
import {
  Divider,
  Typography
} from '@material-ui/core'
import { InfoBox, ListItemBox } from '../info-components'
import EditProfileFields from './edit-profile'
import ProfileImage from './profile-image'

function ProfilePanel ({ ...props }) {
  const { data } = props
  const profileKeys = Object.keys(data)

  return (
    <InfoBox title='Profile'>
      {profileKeys.map((key, index) => {
        const value = data[key] || 'Not specified'
        const title = key.toUpperCase()
        if (key === 'photo') {
          return (
            <div key={index}>
              <ProfileImage
                index={index}
                title={title}
                profileData={data}
                {...props}
              />
            </div>
          )
        }
        return (
          <div key={index}>
            {index !== 0 && <Divider variant='middle' />}
            <ListItemBox
              title={title}
              dataKey={key}
              titleMinWidth={128}
              infoSet='profile'
              EditComponent={EditProfileFields}
              {...props}
            >
              <Typography>{value}</Typography>
            </ListItemBox>
          </div>
        )
      })}
    </InfoBox>
  )
}

export default ProfilePanel
