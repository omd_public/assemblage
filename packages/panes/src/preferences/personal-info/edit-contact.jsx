import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField
} from '@material-ui/core'

function EditContactFields ({ ...props }) {
  const { data, dataKey, handleClose } = props
  const [input, setInput] = React.useState(data[dataKey])

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogContent>
        <DialogTitle>{title.toUpperCase()}</DialogTitle>
        {content}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color='primary'>
          Cancel
        </Button>
        <Button onClick={handleSave} color='primary'>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default EditContactFields
