import * as $rdf from 'rdflib'
import { expand } from '@assemblage/ld'
import { update } from '@assemblage/hooks'

class RdfUtils {
  constructor (store, person, predicates, value) {
    this.store = store
    this.person = person
    this.predicates = predicates
    this.value = value

    this.init()
  }

  init () {
    if (this.person === undefined) {
      throw new Error('Please provide a person to update the graph')
    }
    if (this.predicates === undefined || this.value === undefined) {
      return
    }
    this.personNode = $rdf.sym(this.person)
    this.predicateNodes = this.predicates.map(t => $rdf.sym(expand(t)))
  }

  getStatementsToDelete () {
    return this.predicateNodes.reduce(
      (del, node) => {
        return del
          .concat(
            this.store.statementsMatching(this.personNode, node, null, this.personNode.doc())
          )
      },
      [])
  }

  getStatementsToAdd () {
    return [].concat(
      this.predicateNodes
        .map(node => $rdf.st(this.personNode, node, this.value, this.personNode.doc()))
    )
  }
}

async function uploadFile (store, webId, file, data) {
  const fileName = file.name
  const contentType = file.type
  const person = store.sym(webId)
  const fileNode = store.sym(person.dir().uri + encodeURIComponent(fileName))
  const predicate = store.sym(expand('vcard:hasPhoto'))
  if (store.holds(person, predicate, fileNode)) {
    throw new Error(`A file of name ${fileName} already exists in your profile. Please upload a file with a different name.`)
  }
  try {
    await store.fetcher.webOperation('PUT', fileNode.uri, {
      data: data,
      contentType: contentType
    })
    console.log(' Upload: put OK: ' + fileNode)
    store.add(person, predicate, fileNode, person.doc())
    await store.fetcher.putBack(person.doc(), { contentType: 'text/turtle' })
  } catch (error) {
    throw new Error(`Error at uploading profile image: ${error}`)
  }
}

async function deleteFile (store, updater, subject, predicate, file) {
  const subjectNode = store.sym(subject)
  const predicateNode = store.sym(expand(predicate))
  const fileNode = store.sym(file)
  const statements = profileStore.statementsMatching(subjectNode, predicateNode, fileNode, subjectNode.doc())
  await update(updater, [], statements)
  await store.fetcher.webOperation('DELETE', file)
}

export { RdfUtils, uploadFile, deleteFile }
