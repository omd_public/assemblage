import React from 'react'
import {
  Avatar,
  Divider
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { ListItemBox } from '../info-components'
import EditProfilePhoto from './edit-profile-photo'

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
    'margin-right': theme.spacing(2)
  }
}))

function ProfileImage ({ ...props }) {
  const classes = useStyles()
  const { index, title, profileData, refreshComponent } = props
  const profileImages = [].concat(profileData.photo)

  return (
    <div>
      {index !== 0 && <Divider variant='middle' />}
      <ListItemBox
        title={title}
        dataKey='photo'
        titleMinWidth={128}
        infoSet='profile'
        EditComponent={EditProfilePhoto}
        {...props}
      >
        {profileImages.map((img, i) => {
          return (
            <div key={`profile_img_${i}`}>
              <Avatar className={classes.avatar} alt={profileData.name} src={img} />
            </div>
          )
        })}
      </ListItemBox>
    </div>
  )
}

export default ProfileImage
