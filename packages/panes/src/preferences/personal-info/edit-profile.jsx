import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  TextField,
  Typography
} from '@material-ui/core'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import { makeStyles } from '@material-ui/core/styles'
import { getPredicates } from '@assemblage/ld'
import { update } from '@assemblage/hooks'
import { SessionContext } from '@assemblage/components'
import { RdfUtils } from './rdf-utils'
import LoadingWheel from '../loading-wheel'

const useStyles = makeStyles(theme => ({
  content: {
    'margin-top': theme.spacing(1),
    'margin-bottom': theme.spacing(1)
  },
  gender: {
    'padding-bottom': theme.spacing(1)
  },
  error: {
    color: theme.palette.error.main
  },
  loading: {
    'background-color': 'rgba(211, 211, 211, 0.8)'
  }
}))

function EditProfileFields ({ ...props }) {
  const classes = useStyles()
  const { editorState, handleClose, refreshComponent, profileStore, profileUpdater } = props
  const { open, data, key, context } = editorState
  const [input, setInput] = React.useState(data[key])
  const [updateValues, setUpdateValues] = React.useState({})
  const [updateState, setUpdateState] = React.useState({
    loading: false,
    error: false
  })
  const { session } = React.useContext(SessionContext)
  const webId = session.currentSession.webId
  const title = `Change ${key}`

  React.useEffect(() => {
    async function doUpdate () {
      setUpdateState({ loading: true })
      try {
        await update(profileUpdater, updateValues.ins, updateValues.del)
        setUpdateState({ loading: false, error: false })
        handleClose()
        refreshComponent()
      } catch (error) {
        setUpdateState({ error: true })
      }
    }

    if (updateValues.del && updateValues.ins) {
      doUpdate()
    }
  }, [updateValues])

  function handleSubmit (event) {
    event.preventDefault()
    const predicates = getPredicates(context, key)
    const rdfUtils = new RdfUtils(profileStore, webId, predicates, input)
    setUpdateValues({
      del: rdfUtils.getStatementsToDelete(),
      ins: rdfUtils.getStatementsToAdd()
    })
  }

  function handleChange (event) {
    setInput(event.target.value)
  }

  function handleDateChange (date) {
    setInput(date)
  }

  let content = (
    <TextField
      className={classes.content}
      autoFocus
      id={key}
      onChange={handleChange}
      value={input}
      fullWidth
    />
  )

  if (key === 'gender') {
    content = (
      <FormControl className={classes.content} component='fieldset'>
        <RadioGroup className={classes.gender} name='gender' value={input || ''} onChange={handleChange}>
          <FormControlLabel value='female' control={<Radio />} label='Female' />
          <FormControlLabel value='male' control={<Radio />} label='Male' />
          <FormControlLabel value='other' control={<Radio />} label='Other' />
        </RadioGroup>
      </FormControl>
    )
  }

  if (key === 'birthday') {
    const currentValue = input ? new Date(input) : new Date()
    content = (
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          margin='normal'
          id='date-picker-dialog'
          label='Birthday'
          format='dd/MM/yyyy'
          value={currentValue}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date'
          }}
        />
      </MuiPickersUtilsProvider>
    )
  }

  if (updateState.loading) {
    return <LoadingWheel {...props} />
  }

  return (
    <Dialog
      open={open}
      onClose={handleClose}
    >
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <DialogTitle>{title}</DialogTitle>
          {content}
          {updateState.error &&
            <Typography className={classes.error} variant='caption'>
            There was an error saving your changes :(
            </Typography>}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='primary'>
          Cancel
          </Button>
          <Button type='submit' color='primary'>
          Save
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default EditProfileFields
