import React from 'react'
import {
  Box,
  Typography
} from '@material-ui/core'
import ProfilePanel from './profile-panel'
import ContactPanel from './contact-panel'
import PreferencesPanel from './preferences-panel'

function PersonalInfo ({ data, ...props }) {
  const { profile, contact, preferences } = data

  return (
    <Box display='flex' flexDirection='column'>
      <Box display='flex' flexShrink={1} justifyContent='center' mb={4}>
        <Typography variant='h4'>Personal information</Typography>
      </Box>
      <ProfilePanel data={profile} {...props} />
      <ContactPanel data={contact} {...props} />
      <PreferencesPanel data={preferences} {...props} />
    </Box>
  )
}

export default PersonalInfo
