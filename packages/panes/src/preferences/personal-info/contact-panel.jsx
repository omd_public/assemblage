import React from 'react'
import {
  Box,
  Divider,
  Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { InfoBox, ListItemBox } from '../info-components'

const useStyles = makeStyles(theme => ({
  type: {
    display: 'inline',
    'margin-left': theme.spacing(1),
    'margin-right': theme.spacing(1)
  }
}))

function TypeValueItems ({ data }) {
  const classes = useStyles()

  if (data.length === 0) {
    return <Typography>No phone added</Typography>
  }

  const itemsToShow = data.slice(0, 2)
  const notShown = data.length - 2
  return (
    <Box display='flex' flexDirection='column'>
      {itemsToShow.map(item => {
        return (
          <Box display='flex' alignItems='center' key={item.value}>
            <Typography>{item.value}</Typography>
            {item.type && <Typography variant='caption' color='textSecondary' className={classes.type}> {item.type}</Typography>}
          </Box>
        )
      })}
      {notShown > 0 && <Typography variant='caption' color='textSecondary'> {`+ ${notShown} more`}</Typography>}
    </Box>
  )
}

function AddressItems ({ data }) {
  const first = data[0]

  if (!first) {
    return <Typography>No address added</Typography>
  }

  const { streetAddress, countryName, postalCode, region, locality } = first
  const notShown = data.length - 1
  const separator = ','
  return (
    <Box display='flex' flexDirection='column'>
      <Typography>{streetAddress} {locality}</Typography>
      <Typography>{postalCode} {region}{separator} {countryName}</Typography>
      {notShown > 0 && <Typography variant='caption' color='textSecondary'> {`+ ${notShown} more`}</Typography>}
    </Box>
  )
}

function ContactPanel ({ ...props }) {
  const { data } = props
  return (
    <InfoBox title='Contact Info'>
      <ListItemBox title='EMAIL' dataKey='email' infoSet='profile' titleMinWidth={128} {...props}>
        <TypeValueItems data={data.emails} />
      </ListItemBox>
      <Divider variant='middle' />
      <ListItemBox title='PHONE' dataKey='telephone' infoSet='profile' titleMinWidth={128} {...props}>
        <TypeValueItems data={data.phones} />
      </ListItemBox>
      <Divider variant='middle' />
      <ListItemBox title='ADDRESS' dataKey='address' infoSet='profile' titleMinWidth={128} {...props}>
        <AddressItems data={data.addresses} />
      </ListItemBox>
    </InfoBox>
  )
}

export default ContactPanel
