import React from 'react'
import {
  Box,
  List,
  ListItem,
  Paper,
  Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { ArrowForwardIosRounded } from '@material-ui/icons'

const infoBoxStyle = makeStyles(theme => ({
  paper: {
    borderRadius: theme.spacing(1),
    padding: theme.spacing(2)
  }
}))

export function InfoBox ({ title, children, ...props }) {
  const classes = infoBoxStyle()
  return (
    <Box mb={4}>
      <Paper className={classes.paper}>
        <Box p={3}>
          <Typography variant='h5'>{title}</Typography>
        </Box>
        <List>
          {children}
        </List>
      </Paper>
    </Box>
  )
}

const listItemBoxStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  },
  listBox: {
    display: 'flex',
    'flex-wrap': 'wrap',
    'padding-left': theme.spacing(2),
    'padding-right': theme.spacing(2),
    'align-items': 'center'
  }
}))

export function ListItemBox ({ ...props }) {
  const classes = listItemBoxStyles()
  const { data, title, children, titleMinWidth, dataKey, infoSet, clickHandler, EditComponent } = props

  return (
    <ListItem
      button
      className={classes.root}
      key={dataKey}
      onClick={event => clickHandler(event, data, dataKey, infoSet, EditComponent)}
    >
      <Box display='flex' flexGrow={1}>
        <Box display='flex' flexWrap='wrap' flexGrow={1}>
          <Box className={classes.listBox} minWidth={titleMinWidth}>
            <Typography variant='body2'>{title}</Typography>
          </Box>
          <Box className={classes.listBox}>
            {children}
          </Box>
        </Box>
        <Box className={classes.listBox}>
          <ArrowForwardIosRounded fontSize='small' />
        </Box>
      </Box>
    </ListItem>
  )
}
