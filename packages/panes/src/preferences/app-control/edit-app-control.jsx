import React from 'react'
import {
  Button,
  Dialog,
  DialogActions
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { SessionContext } from '@assemblage/components'
import { update } from '@assemblage/hooks'
import RdfUtils from './rdf-utils'
import EditContent from './edit-content'
import LoadingWheel from '../loading-wheel'

const useStyles = makeStyles(theme => ({
  delete: {
    color: theme.palette.error.main
  }
}))

function EditAppControl ({ ...props }) {
  const classes = useStyles()
  const { editorState, handleClose, refreshComponent, profileStore, profileUpdater } = props
  const { open, data, key } = editorState
  const { session } = React.useContext(SessionContext)
  const { webId } = session.currentSession

  const currentValue = key === 'newApp'
    ? { origin: '', mode: [] }
    : data.find(d => d.origin === key)
  const [input, setInput] = React.useState(currentValue)
  const [updateValues, setUpdateValues] = React.useState({})
  const [updateState, setUpdateState] = React.useState({
    loading: false,
    error: false
  })

  React.useEffect(() => {
    async function doUpdate () {
      setUpdateState({ loading: true })
      try {
        await update(profileUpdater, updateValues.ins, updateValues.del)

        setUpdateState({ loading: false, error: false })
        handleClose()
        refreshComponent()
      } catch (error) {
        console.log('Error updating app control permissions:', error)
        setUpdateState({ error: true, errorMessage: error })
      }
    }

    if (updateValues.ins && updateValues.del) {
      doUpdate()
    }
  }, [updateValues])

  function handleTextChange (event) {
    setInput({ ...input, origin: event.target.value })
  }

  const handleCheckBoxChange = event => {
    const selected = event.target.value
    const updated = input.mode.includes(selected)
      ? input.mode.filter(m => m !== selected)
      : input.mode.concat(selected)
    setInput({ ...input, mode: updated })
  }

  function handleSubmit (event) {
    event.preventDefault()
    const rdfUtils = new RdfUtils(profileStore, webId, input)
    setUpdateValues({
      del: rdfUtils.getStatementsToDelete(),
      ins: rdfUtils.getStatementsToAdd()
    })
  }

  function handleDelete () {
    const rdfUtils = new RdfUtils(profileStore, webId, input)
    setUpdateValues({
      del: rdfUtils.getStatementsToDelete(),
      ins: []
    })
  }

  if (updateState.loading) {
    return <LoadingWheel {...props} />
  }

  return (
    <Dialog
      open={open}
      onClose={handleClose}
    >
      <form onSubmit={handleSubmit}>
        <EditContent
          input={input}
          updateState={updateState}
          handleCheckBoxChange={handleCheckBoxChange}
          handleTextChange={handleTextChange}
          {...props}
        />
        <DialogActions>
          <Button className={classes.delete} onClick={handleDelete}>
          Delete
          </Button>
          <Button onClick={handleClose} color='primary'>
          Cancel
          </Button>
          <Button type='submit' color='primary'>
          Save
          </Button>
        </DialogActions>
      </form>
      {updateState.error &&
        <Typography className={classes.error} variant='caption'>
          {updateState.errorMessage}
        </Typography>}
    </Dialog>
  )
}

export default EditAppControl
