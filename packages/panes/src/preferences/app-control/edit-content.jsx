import React from 'react'
import {
  Box,
  Checkbox,
  CircularProgress,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormGroup,
  TextField,
  Tooltip,
  Typography
} from '@material-ui/core'
import { HelpRounded } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'

const ACCESS_LEVELS = ['Read', 'Append', 'Write', 'Control']
const ACCESS_LEVEL_DESCRIPTIONS = {
  read: 'App has access to read the contents of your pod.',
  append: 'App has access to add content to your pod but not modify or remove them.',
  write: 'App has access to add, modify and delete the contents of your pod.',
  control: 'App has access to modify the Access Control List of your pod items.'
}

const useStyles = makeStyles(theme => ({
  checkBox: {
    'margin-right': theme.spacing(1)
  },
  icon: {
    height: theme.spacing(2),
    width: theme.spacing(2),
    color: theme.palette.grey[500]
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  error: {
    color: theme.palette.error.main
  }
}))

function EditContent ({ ...props }) {
  const classes = useStyles()
  const {
    input,
    updateState,
    handleCheckBoxChange,
    handleTextChange
  } = props
  const title = 'Application access settings'

  return (
    <DialogContent>
      <DialogTitle>{title}</DialogTitle>
      <Box pt={1} pb={1}>
        <Typography variant='subtitle2'>Application URL</Typography>
        <TextField
          autoFocus
          onChange={handleTextChange}
          value={input.origin}
          fullWidth
          type='url'
          required
        />
      </Box>
      <Box pt={1} pb={1}>
        <FormControl component='fieldset'>
          <Typography variant='subtitle2'>Access modes</Typography>
          <FormGroup>
            {ACCESS_LEVELS.map((level, index) => (
              <Box display='flex' alignItems='center' key={index}>
                <FormControlLabel
                  className={classes.checkBox}
                  key={index}
                  control={<Checkbox
                    checked={input.mode.includes(level)}
                    onChange={handleCheckBoxChange}
                    value={level}
                    color='primary'
                           />}
                  label={level}
                />
                <Tooltip title={ACCESS_LEVEL_DESCRIPTIONS[level.toLowerCase()]} placement='right'>
                  <HelpRounded className={classes.icon} />
                </Tooltip>
              </Box>
            ))}
          </FormGroup>
        </FormControl>
      </Box>
      {updateState.error &&
        <Typography className={classes.error} variant='caption'>
            There was an error saving your changes :(
        </Typography>}
      {updateState.loading &&
        <CircularProgress className={classes.buttonProgress} />}
    </DialogContent>
  )
}

export default EditContent
