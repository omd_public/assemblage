import React from 'react'
import {
  Box,
  Button,
  Divider,
  Typography
} from '@material-ui/core'
import { InfoBox, ListItemBox } from '../info-components'
import { CheckCircleOutlineRounded, NotInterestedRounded } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import EditAppControl from './edit-app-control'

const ACCESS_LEVELS = ['Read', 'Append', 'Write', 'Control']

const useStyles = makeStyles(theme => ({
  greenIcon: {
    color: 'green',
    'margin-right': theme.spacing(0.5)
  },
  greyIcon: {
    color: 'grey',
    'margin-right': theme.spacing(0.5)
  }
}))

function AccessIcon ({ level, modes }) {
  const classes = useStyles()
  return modes.includes(level)
    ? <CheckCircleOutlineRounded className={classes.greenIcon} fontSize='small' />
    : <NotInterestedRounded className={classes.greyIcon} fontSize='small' />
}

function AppControl ({ clickHandler, ...props }) {
  const { data } = props
  return (
    <Box display='flex' flexDirection='column'>
      <Box display='flex' flexShrink={1} flexDirection='column' mb={4}>
        <Box display='flex' justifyContent='center' mb={1}>
          <Typography variant='h4'>App Control</Typography>
        </Box>
        <Box display='flex' justifyContent='center'>
          <Typography variant='subtitle1'>Control which applications have access to your pod.</Typography>
        </Box>
      </Box>
      <InfoBox title='Trusted Applications'>
        {data.map((app, index) => {
          return (
            <div key={index}>
              {index !== 0 && <Divider variant='middle' />}
              <ListItemBox
                title={app.origin}
                dataKey={app.origin}
                titleMinWidth={256}
                infoSet='profile'
                clickHandler={clickHandler}
                EditComponent={EditAppControl}
                {...props}
              >
                {ACCESS_LEVELS.map((level, i) => {
                  return (
                    <Box display='flex' alignItems='center' mr={2} key={i}>
                      <AccessIcon level={level} modes={app.mode} />
                      <Typography>{level}</Typography>
                    </Box>
                  )
                })}
              </ListItemBox>
            </div>
          )
        })}
        <Box p={2}>
          <Button
            variant='contained'
            color='primary'
            fullWidth
            onClick={event => clickHandler(event, data, 'newApp', 'appControl', EditAppControl)}
          >
          Add new trusted application
          </Button>
        </Box>
      </InfoBox>
    </Box>
  )
}

export default AppControl
