import * as $rdf from 'rdflib'
import { expand } from '@assemblage/ld'

class RdfUtils {
  constructor (store, person, app) {
    this.store = store
    this.person = person
    this.app = app

    this.init()
  }

  init () {
    if (this.person === undefined) {
      throw new Error('Please provide a person to update the graph')
    }
    if (this.app === undefined) {
      throw new Error('No app is defined to be updated')
    }
    this.personNode = $rdf.sym(this.person)
    this.appUrl = $rdf.sym(this.app.origin)
  }

  getStatementsToDelete () {
    const statements = this.store.statementsMatching(null, $rdf.sym(expand('acl:origin')), this.appUrl, null)
    return statements.reduce(
      (del, st) => {
        return del
          .concat(
            this.store.statementsMatching(
              this.personNode,
              $rdf.sym(expand('acl:trustedApp')),
              st.subject,
              null
            )
          )
          .concat(
            this.store.statementsMatching(
              st.subject,
              null,
              null,
              null
            )
          )
      },
      []
    )
  }

  getStatementsToAdd () {
    const application = new $rdf.BlankNode()
    return [
      $rdf.st(this.personNode, $rdf.sym(expand('acl:trustedApp')), application, this.personNode.doc()),
      $rdf.st(application, $rdf.sym(expand('acl:origin')), this.appUrl, this.personNode.doc()),
      ...this.app.mode
        .map(m => $rdf.sym(expand(`acl:${m}`)))
        .map(m => $rdf.st(application, $rdf.sym(expand('acl:mode')), m, this.personNode.doc()))
    ]
  }
}

export default RdfUtils
