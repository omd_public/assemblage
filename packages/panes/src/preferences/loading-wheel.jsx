import React from 'react'
import {
  CircularProgress,
  Dialog
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  progress: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
    overflow: 'hidden'
  },
  buttonProgress: {
    color: 'white'
  }
}))

function LoadingWheel ({ ...props }) {
  const classes = useStyles()

  return (
    <Dialog
      PaperProps={{
        classes: {
          root: classes.progress
        }
      }}
      open
      onClose={() => { }}
    >
      <CircularProgress className={classes.buttonProgress} size={68} />
    </Dialog>
  )
}

export default LoadingWheel
