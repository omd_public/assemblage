import he from 'he'
import { infoSets, toInfoSet } from '../profile/profile-infosets'

class ProfileOrganiser {
  constructor (data, preferences, store) {
    this.data = data
    this.preferences = preferences
    this.store = store
    this.organisedProfile = {}
  }

  organise () {
    this.organisedProfile.personal = this._getPersonalInfo()
    this.organisedProfile.appControl = this._getAppControlInfo()
    return this.organisedProfile
  }

  _getPersonalInfo () {
    return {
      profile: this._getProfileInfo(),
      contact: this._getContactInfo(),
      preferences: this._toPreferencesValues()
    }
  }

  _getProfileInfo () {
    const name = he.decode(this.data.name || '')
    const {
      photo,
      role,
      org,
      gender,
      birthday
    } = this.data
    return {
      name,
      photo,
      role,
      org,
      gender,
      birthday
    }
  }

  _getContactInfo () {
    const {
      telephone,
      email,
      address
    } = this.data

    return {
      phones: this._toTypeValueObject(telephone, 'tel:'),
      addresses: toInfoSet(this.store, address, infoSets.address),
      emails: this._toTypeValueObject(email, 'mailto:')
    }
  }

  _getAppControlInfo () {
    const results = toInfoSet(this.store, this.data.trustedApps, infoSets.apps)
    return results.map(res => {
      const mode = Array.isArray(res.mode)
        ? res.mode.map(m => m.split('#')[1])
        : this._handleSingleOrUndefinedMode(res.mode)
      return {
        origin: res.origin,
        mode
      }
    })
  }

  _handleSingleOrUndefinedMode (mode) {
    return mode ? [].concat(mode.split('#')[1]) : []
  }

  _toTypeValueObject (subject, replaceFragment) {
    const results = toInfoSet(this.store, subject, infoSets.typeValuePair)
    return results.map(res => {
      const { type, value } = res
      const transformedType = type ? type.split('#')[1] : null
      return {
        type: transformedType,
        value: value.replace(replaceFragment, '')
      }
    })
  }

  _toPreferencesValues () {
    const { mBox, userType } = this.preferences
    let type = 'Not Specified'
    if (userType) {
      const userTypeStr = userType.split('#')[1]
      type = userTypeStr.match(/[A-Z][a-z]+/g).join(' ')
    }

    return {
      mBox: mBox.replace('mailto:', ''),
      userType: type
    }
  }
}

export default ProfileOrganiser
