import React from 'react'

import {
  Backdrop,
  CircularProgress,
  Typography,
  makeStyles
} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff'
  },
  message: {
    marginLeft: theme.spacing(1)
  }
}))

export default function WaitBackdrop ({ text, open }) {
  const classes = useStyles()

  return (
    <Backdrop className={classes.backdrop} open={open}>
      <CircularProgress color='inherit' />
      <Typography className={classes.message}>{text}</Typography>
    </Backdrop>
  )
}
