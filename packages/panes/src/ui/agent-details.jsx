import React from 'react'

import {
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar
} from '@material-ui/core'

import { Mail as MailIcon, WebAsset as AppIcon } from '@material-ui/icons'

import he from 'he'
import { profileInfo, createFetcher, loadRDF, single } from '@assemblage/ld'

import { ListItemSkeleton } from '.'

function AlternativeProfile ({ subjectUrl, children }) {
  const profile = {
    name: subjectUrl,
    icon: <AppIcon />
  }

  if (subjectUrl.startsWith('mailto:')) {
    profile.name = subjectUrl.substr(7)
    profile.icon = <MailIcon />
  }

  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar alt={profile.name}>
          {profile.icon}
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={profile.name}
      />
      {children}
    </ListItem>
  )
}

export default function AgentDetails (props) {
  const { subjectUrl, store, variant, children } = props
  const [profile, setProfile] = React.useState()
  const [altProfile, setAltProfile] = React.useState(false)

  React.useEffect(() => {
    let running = true
    const getProfile = async () => {
      let profile = profileInfo({
        store, subjectUrl
      })
      if (profile) {
        profile.webId = subjectUrl
        setProfile(profile)
      } else {
        // Load from its own source
        try {
          const fetcher = createFetcher(store)
          await loadRDF(fetcher, subjectUrl)
          if (running) {
            profile = profileInfo({
              store, subjectUrl
            }) || { name: '???' }
            profile.webId = subjectUrl
            setProfile(profile)
          }
        } catch (error) {
          if (running) {
            setProfile({
              webId: subjectUrl,
              name: '???'
            })
          }
        }
      }
    }

    if (subjectUrl.startsWith('http')) {
      // TODO handle origin
      getProfile()
    } else {
      setAltProfile(true)
    }

    return () => {
      running = false
    }
  }, [])

  if (altProfile) {
    return <AlternativeProfile {...props} />
  }

  if (profile) {
    const name = he.decode(single(profile.name) || single(profile.nickname) || '')
    const link = profile.homepage || profile.webId

    if (variant === 'group') {
      return (
        <Avatar className='MuiAvatarGroup-avatar' alt={name} src={single(profile.photo)} />
      )
    }

    return (
      <ListItem
        button onClick={() => {
          window.location.href = link
        }}
      >
        <ListItemAvatar>
          <Avatar alt={name} src={single(profile.photo)} />
        </ListItemAvatar>
        <ListItemText
          primary={name}
          secondary={link}
        />
        {children}
      </ListItem>)
  }

  return <ListItemSkeleton />
}
