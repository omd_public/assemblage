import React from 'react'

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button
} from '@material-ui/core'

export default function ConfirmationDialog (props) {
  const { onCancel, onOk, title, text, open, ...other } = props

  const handleCancel = event => {
    onCancel(event)
  }

  const handleOk = event => {
    onOk(event)
  }

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth='xs'
      aria-labelledby='confirmation-dialog'
      open={open}
      {...other}
    >
      <DialogTitle id='confirmation-dialog'>{title}</DialogTitle>
      <DialogContent dividers>
        {text}
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleCancel} color='primary'>
          Cancel
        </Button>
        <Button onClick={handleOk} color='primary'>
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  )
}
