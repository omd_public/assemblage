import React from 'react'

import {
  Box
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'

export default function () {
  return (
    <Box display='flex' py={1} px={2}>
      <Skeleton variant='circle' width={40} height={40} />
      <Box ml={2} flexGrow={1}>
        <Skeleton width='100%' height={40} />
      </Box>
    </Box>
  )
}
