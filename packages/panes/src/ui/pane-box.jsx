import React from 'react'

import {
  Box,
  Paper,
  Typography,
  Divider
} from '@material-ui/core'

export default function PaneBox ({ title, appbar, footer, children }) {
  return (
    <Box mb={2}>
      <Paper>
        <Box p={2}>
          <Box display='flex' flexWrap='wrap'>
            <Typography variant='h5' component='h1'>{title}</Typography>
            {appbar && appbar}
          </Box>
          <Box my={1}>
            {children}
          </Box>
        </Box>
        {footer &&
          <>
            <Divider />
            <Box py={1} px={1}>
              {footer}
            </Box>
          </>}
      </Paper>
    </Box>
  )
}
