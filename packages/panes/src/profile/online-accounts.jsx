import React from 'react'

import { Box, Link, Avatar } from '@material-ui/core'
import { LinkedIn, YouTube, LibraryMusic, AccountBalanceWallet, Dashboard, GitHub, Pinterest, CollectionsBookmark, LocalCafe, Twitter, Facebook, Instagram, PhotoLibrary, ContactPhone, EmojiPeople } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  account: {
    marginRight: theme.spacing(0.5),
    marginBottom: theme.spacing(1)
  }
}))

const iconUrlMap = [
  {
    regexp: /youtube\./i,
    icon: <YouTube />
  },
  {
    regexp: /last\.fm/i,
    icon: <LibraryMusic />
  },
  {
    regexp: /linkedin\.com/i,
    icon: <LinkedIn />
  },
  {
    regexp: /twine\.com/i,
    icon: <Dashboard />
  },
  {
    regexp: /pinterest\.com/i,
    icon: <Pinterest />
  },
  {
    regexp: /bitcoin\./i,
    icon: <AccountBalanceWallet />
  },
  {
    regexp: /github\.com/i,
    icon: <GitHub />
  },
  {
    regexp: /twitter\.com/i,
    icon: <Twitter />
  },
  {
    regexp: /flickr\.com/i,
    icon: <PhotoLibrary />
  },
  {
    regexp: /delicious\.com/i,
    icon: <CollectionsBookmark />
  },
  {
    regexp: /skype\.com/i,
    icon: <ContactPhone />
  },
  {
    regexp: /java\.net/i,
    icon: <LocalCafe />
  },
  {
    regexp: /facebook\.com/i,
    icon: <Facebook />
  },
  {
    regexp: /instagram\.com/i,
    icon: <Instagram />
  }
]

function iconFromUrl (url) {
  for (let i = 0; i < iconUrlMap.length; i++) {
    const m = iconUrlMap[i]

    if (url.match(m.regexp)) {
      return m.icon
    }
  }
  return <EmojiPeople />
}

function OnlineAccounts ({ data }) {
  if (data) {
    const classes = useStyles()
    return (
      <Box display='flex' flexWrap='wrap' mt={2}>
        {[].concat(data).map((account, i) => (
          <Link key={i} href={account.link}>
            <Avatar alt={account.label} className={classes.account}>
              {iconFromUrl(account.link)}
            </Avatar>
          </Link>
        ))}
      </Box>
    )
  }
  return null
}

export default OnlineAccounts
