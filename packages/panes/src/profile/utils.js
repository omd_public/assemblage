export function ellipsis (str) {
  if (str.length > 30) {
    return str.substr(0, 17) + '...' + str.substr(str.length - 10, str.length)
  }
  return str
}
