import React from 'react'

import {
  Box,
  Typography,
  Divider,
  Link
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import he from 'he'
import { useReify } from '@assemblage/hooks'
import { profileInfo, single } from '@assemblage/ld'

import { infoSets, toInfoSet } from './profile-infosets'
import { Address, Email, Phone, DataExpansionPanel } from './data-panel'
import Friends from './friends'
import OnlineAccounts from './online-accounts'
import { ellipsis } from './utils'

const useStyles = makeStyles(theme => ({
  profileImg: {
    width: '100%',
    borderRadius: '5px',
    objectPosition: 'top',
    objectFit: 'cover'
  },
  wrap: {
    margin: '0 auto',
    maxWidth: '100%'
  },
  browserVisible: {
    display: 'none'
  },
  [theme.breakpoints.up('sm')]: {
    browserVisible: {
      display: 'inherit'
    },
    profileImg: {
      width: '260px',
      maxHeight: '260px'
    },
    wrap: {
      marginRight: theme.spacing(2),
      marginLeft: 0,
      maxWidth: '260px'
    },
    leftWrap: {
      marginRight: 0,
      marginLeft: theme.spacing(2)
    }
  }
}))

function Links ({ title, data }) {
  if (data) {
    return (
      <Box my={1}>
        <Typography>{title}</Typography>
        {[].concat(data).map((d, i) => (
          <Link key={i} href={d}><Typography>{ellipsis(d)}</Typography></Link>
        ))}
      </Box>)
  }
  return null
}

function Profile ({ subjectUrl, store, children }) {
  const classes = useStyles()

  const state = useReify({
    pro: profileInfo
  }, {
    subjectUrl,
    store
  })

  if (state.loading) {
    return null
  }

  if (state.error) {
    return null
  }

  const entity = state.it
  const pro = entity.pro
  const entityStore = entity.store
  const name = he.decode(single(pro.name) || single(pro.nickname) || '')
  const {
    photo,
    role,
    org,
    note,
    homepage,
    weblog,
    knows
  } = pro
  const accounts = toInfoSet(entityStore, pro.account, infoSets.account)
  const phones = toInfoSet(entityStore, pro.telephone, infoSets.typeValuePair)
  const addresses = toInfoSet(entityStore, pro.address, infoSets.address)
  const mbox = pro.mbox && [].concat(pro.mbox).map(m => ({ value: m }))
  const emails = toInfoSet(entityStore, pro.email, infoSets.typeValuePair)

  return (
    <Box mb={4}>
      <Box display='flex' flexWrap='wrap' py={2}>
        <Box display='flex' flexWrap='wrap' className={classes.wrap}>
          <Box flexGrow={1} mb={2}>
            {photo &&
              <img src={single(photo)} className={classes.profileImg} />}
            <Box my={1}>
              <Typography variant='h5' component='h1'>{name}</Typography>
              <Box mt={0.5} mb={1}>
                {role && <Typography color='textSecondary'>{role}</Typography>}
                {org && <Typography color='textSecondary'>{org}</Typography>}
              </Box>
            </Box>
            <div className={classes.browserVisible}>
              <OnlineAccounts data={accounts} />
              <Box my={1}>
                <DataExpansionPanel
                  data={[].concat(mbox).concat(emails)}
                  aria-controls='mail-content'
                  id='mail-header'
                  element={Email}
                />

                <DataExpansionPanel
                  data={[].concat(phones)}
                  aria-controls='phone-content'
                  id='phone-header'
                  element={Phone}
                />

                <DataExpansionPanel
                  data={[].concat(addresses)}
                  aria-controls='address-content'
                  id='address-header'
                  element={Address}
                />
              </Box>
              <Links title='Homepage' data={homepage} />
              <Links title='Weblog' data={weblog} />
              {note &&
                <>
                  <Divider />
                  <Box my={1}>
                    <Typography>{note}</Typography>
                  </Box>
                </>}
            </div>
          </Box>
        </Box>
        <Box flexGrow={1} className={classes.leftWrap}>
          {children}
          {knows &&
            <Friends store={entityStore} knows={knows} />}
        </Box>
      </Box>
    </Box>
  )
}

export default Profile
