import React from 'react'

import {
  Box,
  Typography,
  ExpansionPanel as MuiExpansionPanel,
  ExpansionPanelSummary as MuiExpansionPanelSummary,
  ExpansionPanelDetails as MuiExpansionPanelDetails,
  Link
} from '@material-ui/core'

import {
  ExpandMore,
  ContactPhoneOutlined as ContactPhoneIcon,
  MailOutlined as MailIcon
} from '@material-ui/icons'

import { makeStyles, withStyles } from '@material-ui/core/styles'

import { ellipsis } from './utils'

const useStyles = makeStyles(theme => ({
  m: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5)
  }
}))

const ExpansionPanel = withStyles({
  root: {
    border: 'none',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0
    },
    '&:before': {
      display: 'none'
    },
    '&$expanded': {
      margin: 'auto'
    }
  },
  expanded: {}
})(MuiExpansionPanel)
const ExpansionPanelSummary = withStyles({
  root: {
    margin: 0,
    minHeight: 0,
    padding: 0,
    '&$expanded': {
      minHeight: 0
    }
  },
  content: {
    minHeight: 0,
    '&$expanded': {
      margin: 0
    }
  },
  expanded: {}
})(MuiExpansionPanelSummary)

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(0)
  }
}))(MuiExpansionPanelDetails)

function TypeLabel ({ type }) {
  if (type) {
    return (
      <Typography variant='caption' color='textSecondary' display='inline'> {type.split('#')[1]}</Typography>
    )
  }
  return null
}

export function Email ({ item, ...props }) {
  const classes = useStyles()
  const { value, type } = item
  const label = value.replace('mailto:', '')
  return (
    <Box display='flex' alignItems='center' {...props}>
      <MailIcon />
      <Link href={value} className={classes.m}>
        <Typography>{ellipsis(label)}</Typography>
      </Link>
      <TypeLabel type={type} />
    </Box>
  )
}

export function Phone ({ item, ...props }) {
  const classes = useStyles()
  const { value, type } = item
  const label = value.replace('tel:', '')
  return (
    <Box display='flex' alignItems='center' {...props}>
      <ContactPhoneIcon />
      <Link href={value} className={classes.m}>
        <Typography>{ellipsis(label)}</Typography>
      </Link>
      <TypeLabel type={type} />
    </Box>
  )
}

export function Address ({ item, ...props }) {
  const { type, streetAddress, countryName, postalCode, region, locality } = item
  return (
    <Box display='flex' flexDirection='column' mb={2} {...props}>
      <Typography>
        {streetAddress}
        <TypeLabel type={type} />
      </Typography>
      <Typography>
        {postalCode} {locality} {region} {countryName}
      </Typography>
    </Box>
  )
}

export function DataExpansionPanel ({ data, element, ...props }) {
  const Component = element
  const filtered = data.filter(x => x !== undefined)

  if (filtered.length === 0) {
    return null
  }

  const first = filtered.shift()

  if (filtered.length === 0) {
    return (
      <Box mb={1}>
        <Component item={first} />
      </Box>
    )
  }

  return (
    <ExpansionPanel>
      <ExpansionPanelSummary
        expandIcon={<ExpandMore />}
        {...props}
      >
        <Component item={first} />
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Box display='flex' flexDirection='column'>
          {filtered.map((item, i) => (
            <Component key={i} item={item} mb={1} />
          ))}
        </Box>
      </ExpansionPanelDetails>

    </ExpansionPanel>
  )
}
