import React from 'react'

import {
  Box,
  List,
  Divider,
  Avatar
} from '@material-ui/core'
import {
  AvatarGroup,
  Pagination
} from '@material-ui/lab'

import { PaneBox, AgentDetails } from '../ui'

function Friends (props) {
  const { knows, store, variant } = props

  // To handle profiles with only 1 friend
  const knowsAsArray = [].concat(knows)

  // TODO batches, throttling...
  const numOfPages = Math.round(knowsAsArray.length / 12)
  const len = knowsAsArray.length > 12 ? 12 : knowsAsArray.length
  const rest = knowsAsArray.length - len
  const friends = knowsAsArray.slice(0, len)

  if (variant === 'group') {
    return (
      <PaneBox title='Friends'>
        <AvatarGroup>
          {friends.map((friend, i) => (
            <AgentDetails
              key={i}
              store={store}
              subjectUrl={friend}
              variant={variant}
            />
          ))}
          {rest > 0 &&
            <Avatar>+{rest}</Avatar>}
        </AvatarGroup>
      </PaneBox>
    )
  }

  return (
    <PaneBox
      title='Friends' footer={numOfPages > 0 &&
        <Box display='flex' alignItems='center' justifyContent='center'>
          <Pagination count={numOfPages} shape='rounded' />
        </Box>}
    >
      <List>
        {friends.map((friend, i) => (
          <div key={i}>
            <AgentDetails
              store={store}
              subjectUrl={friend}
            />
            {i < len - 1 && <Divider variant='inset' />}
          </div>
        ))}
      </List>
    </PaneBox>
  )
}

export default Friends
