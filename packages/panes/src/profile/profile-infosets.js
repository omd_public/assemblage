import { infoSet } from '@assemblage/ld'

export const infoSets = {
  typeValuePair: [
    { name: 'type', predicates: ['rdf:type'] },
    { name: 'value', predicates: ['vcard:value'] }
  ],
  account: [
    { name: 'label', predicates: ['rdfs:label'] },
    { name: 'accountName', predicates: ['foaf:accountName'] },
    {
      name: 'link',
      predicates: [
        'foaf:accountProfilePage',
        'foaf:homepage',
        'foaf:accountServiceHomepage'
      ]
    }
  ],
  apps: [
    { name: 'origin', predicates: ['acl:origin'] },
    { name: 'mode', predicates: ['acl:mode'] }
  ],
  address: [
    { name: 'streetAddress', predicates: ['vcard:street-address'] },
    { name: 'countryName', predicates: ['vcard:country-name'] },
    { name: 'region', predicates: ['vcard:region'] },
    { name: 'postalCode', predicates: ['vcard:postal-code'] },
    { name: 'locality', predicates: ['vcard:locality'] }
  ]
}

export function toInfoSet (store, subjectUrls, iset) {
  if (subjectUrls === undefined) {
    return []
  }
  const _subjectUrls = [].concat(subjectUrls)
  const result = []
  for (let i = 0; i < _subjectUrls.length; i++) {
    const subjectUrl = _subjectUrls[i]
    const val = infoSet({
      store,
      subjectUrl
    }, iset)

    if (val) {
      result.push(val)
    }
  }
  return result
}
