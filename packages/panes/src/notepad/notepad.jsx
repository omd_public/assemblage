import React from 'react'

import {
  Box,
  Paper
} from '@material-ui/core'
import { Breadcrumbs } from '@assemblage/components'
import { useReify } from '@assemblage/hooks'

import PadEditor from './editor'

export const NotepadContext = React.createContext()

/**
 * Notepad pane.
 *
 * Provides context and the initial loading for the editor.
 *
 * @param {*} props
 */
export default function (props) {
  const { store, fetcher, updater, subjectUrl } = props
  const state = useReify({
    created: ['dc:created'],
    author: ['dc:author']
  },
  props)

  if (state.loading) {
    return null
  }

  const doc = store.sym(subjectUrl).doc()
  const entity = state.it

  return (
    <NotepadContext.Provider
      value={{ entity, store, fetcher, updater, subjectUrl, doc }}
    >
      <Box mb={1}>
        <Breadcrumbs
          subjectUrl={subjectUrl}
          maxItems={5}
          view='browser'
          aria-label='breadcrumbs'
        />
      </Box>
      <Paper>
        <PadEditor />
      </Paper>
    </NotepadContext.Provider>
  )
}
