
import { anyObject, eachObject } from '@assemblage/ld'

export function readTitle ({ store, subjectUrl }) {
  const title = anyObject(store, subjectUrl, 'dc:title')
  return title ? title.value : 'untitled'
}

export function readLine ({ store, subject }) {
  const content = anyObject(store, subject, 'sioc:content')
  const author = anyObject(store, subject, 'dc:author')
  const next = anyObject(store, subject, 'pad:next')
  const indent = anyObject(store, subject, 'pad:indent')

  return {
    subject,
    content: content && content.value,
    author: author.value,
    indent,
    next
  }
}

export function readLines ({ store, subjectUrl, lines = [] }) {
  const nextLine = anyObject(store, subjectUrl, 'pad:next')
  const line = readLine({ store, subject: nextLine })

  // Checks stop condition
  if (nextLine && !nextLine.value.endsWith('#this')) {
    lines.push(line)

    return readLines({
      store,
      subjectUrl: nextLine.value,
      lines
    })
  }

  return lines
}

export function readParticipation ({ store, subjectUrl }) {
  const data = []
  const map = {}
  const participations = eachObject(store, subjectUrl, 'wf:participation')

  for (let i = 0; i < participations.length; i++) {
    const p = participations[i]
    const participant = anyObject(store, p, 'wf:participant')
    const bgColorNode = anyObject(store, p, 'ui:backgroundColor')
    const psubj = participant && participant.value
    const bgColor = bgColorNode ? bgColorNode.value : '#CCCCCC'
    if (psubj) {
      data.push({
        participant: psubj,
        bgColor
      })
      map[psubj] = {
        bgColor
      }
    }
  }
  return {
    data,
    map
  }
}
