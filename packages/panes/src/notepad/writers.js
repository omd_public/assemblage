import * as $rdf from 'rdflib'

import { slugify } from '@assemblage/utils'
import { asSym, anyObject, anySubject, ns } from '@assemblage/ld'

import auth from 'solid-auth-client'
import FileClient from 'solid-file-client'

const fileClient = new FileClient(auth)

function newThing (doc) {
  const now = new Date()
  return $rdf.sym(doc.uri + '#' + 'id' + ('' + now.getTime()))
}

/**
 * Returns a color from a given string.
 *
 * @param {String} str The string to be hashed
 * @param {Number} s The saturation percentage
 * @param {Number} l The lightness percentage
 */
function stringToHslColor (str, s = 30, l = 80) {
  var hash = 0
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash)
  }

  var h = hash % 360
  return 'hsl(' + h + ', ' + s + '%, ' + l + '%)'
}

export function newLineStatements ({
  store, doc, currentLine, me
}) {
  const subject = asSym(currentLine.subject)
  const next = anyObject(store, subject, 'pad:next')
  const del = [$rdf.st(subject, ns('pad:next'), next, doc)]

  const newLine = newThing(doc)
  const author = $rdf.sym(me)

  const ins = [
    $rdf.st(subject, ns('pad:next'), newLine, doc),
    $rdf.st(newLine, ns('pad:next'), next, doc),
    $rdf.st(newLine, ns('dc:author'), author, doc),
    $rdf.st(newLine, ns('sioc:content'), '', doc)
  ]
  if (currentLine.indent) {
    ins.push($rdf.st(newLine, ns('pad:indent'), currentLine.indent, doc))
  }

  return {
    del,
    ins,
    newLine
  }
}

export function deleteLineStatements ({
  store,
  doc,
  currentLine
}) {
  const { subject } = currentLine
  const prev = anySubject(store, 'pad:next', subject)
  const next = anyObject(store, subject, 'pad:next')

  if (prev.sameTerm(subject) && next.sameTerm(subject)) {
    return {}
  }

  const del = store
    .statementsMatching(subject, undefined, undefined, doc)
    .concat(store.statementsMatching(undefined, undefined, subject, doc))
  const ins = [$rdf.st(prev, ns('pad:next'), next, doc)]

  return {
    del,
    ins
  }
}

export function newParticipationStatements ({
  doc, subjectUrl, me
}) {
  const participation = newThing(doc)
  const ins = [
    $rdf.st($rdf.sym(subjectUrl), ns('wf:participation'), participation, doc),
    $rdf.st(participation, ns('wf:participant'), $rdf.sym(me), doc),
    $rdf.st(participation, ns('cal:dtstart'), new Date(), doc),
    $rdf.st(
      participation,
      ns('ui:backgroundColor'),
      stringToHslColor(me),
      doc
    )
  ]
  return {
    ins,
    newParticipation: participation
  }
}

export function updateContentStatements ({ doc, currentLine, newValue }) {
  const del = [$rdf.st(currentLine.subject, ns('sioc:content'), currentLine.content, doc)]
  const ins = [$rdf.st(currentLine.subject, ns('sioc:content'), newValue, doc)]

  return {
    del,
    ins
  }
}

/**
 *
 * @param {*} param0
 */
export function newNotepad ({ docUri, title, me }) {
  const doc = $rdf.sym(docUri)
  const graph = $rdf.graph()
  const subjectUrl = `${docUri}#this`
  const subject = $rdf.sym(subjectUrl)
  const author = $rdf.sym(me)

  const { ins } = newParticipationStatements({ me, doc, subjectUrl })

  graph.add(subject, ns('rdf:type'), ns('pad:Notepad'))
  graph.add(subject, ns('dc:title'), $rdf.lit(title))
  graph.add(subject, ns('dc:created'), new Date())
  graph.add(subject, ns('dc:author'), author)

  const start = $rdf.sym(`${docUri}#this_start`)
  graph.add(start, ns('dc:author'), author)
  graph.add(start, ns('sioc:content'), $rdf.lit(''))
  graph.add(start, ns('pad:next'), subject)

  graph.add(subject, ns('pad:next'), start)

  ins.forEach(stmt => {
    graph.addStatement(stmt)
  })

  return $rdf.serialize(null, graph, docUri, 'text/turtle')
}

export async function createNewNotepad ({
  folder,
  title,
  me,
  onSuccess,
  onError
}) {
  const baseUri = folder.endsWith('/') ? folder : folder + '/'
  const docUri = baseUri + `${slugify(title)}.ttl`
  const fileContent = newNotepad({ me, docUri, title })

  try {
    const res = await fileClient.createFile(docUri, fileContent, 'text/turtle')

    if (res.ok) {
      onSuccess(docUri)
    } else {
      onError(res)
    }
  } catch (error) {
    onError(error)
  }
}
