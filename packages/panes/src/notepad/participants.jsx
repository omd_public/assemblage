import React from 'react'

import {
  Box,
  Avatar,
  Badge,
  Tooltip,
  withStyles
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { profileInfo, loadRDF, single } from '@assemblage/ld'

import { NotepadContext } from './notepad'

function createBadge (color) {
  return withStyles(theme => ({
    badge: {
      backgroundColor: color,
      color,
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`
    }
  }))(Badge)
}

async function loadParticipantProfiles ({ store, fetcher, participations }) {
  const data = []
  for (let i = 0; i < participations.length; i++) {
    const p = participations[i]
    const participant = p.participant
    await loadRDF(fetcher, participant)
    const participantDetails = profileInfo({ store, subjectUrl: participant })
    participantDetails.participant = participant
    data.push(
      participantDetails
    )
  }
  return data
}

export default function ParticipantList ({ participations }) {
  const { store, fetcher } = React.useContext(NotepadContext)
  const [participationState, setParticipationState] = React.useState({
    loading: true
  })

  React.useEffect(() => {
    // TODO handle unmounted
    const load = async () => {
      try {
        const profiles = await loadParticipantProfiles({ store, fetcher, participations })
        setParticipationState({
          loading: false,
          profiles
        })
      } catch (error) {
        console.error(error)
      }
    }
    load()
  }, [participations])

  if (participationState.loading) {
    return (
      <Box m={2}>
        <Skeleton variant='circle' width={40} height={40} />
      </Box>)
  }

  const { profiles } = participationState

  return (
    <Box m={2}>
      {participations.map((party, i) => {
        const ParticipantBadge = createBadge(party.bgColor)
        const profile = profiles.find(p => p.participant === party.participant)

        if (!profile) {
          return null
        }

        return (
          <Box key={i} my={2}>
            <Tooltip title={profile.name}>
              <ParticipantBadge
                overlap='circle'
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right'
                }}
                variant='dot'
                badgeContent=' '
              >
                <Avatar src={single(profile.photo)} alt={profile.name} />
              </ParticipantBadge>
            </Tooltip>
          </Box>
        )
      })}
    </Box>)
}
