import React from 'react'

import {
  Box,
  Typography,
  IconButton,
  Tooltip,
  Divider
} from '@material-ui/core'
import {
  Edit as EditIcon,
  Visibility as ViewIcon
} from '@material-ui/icons'
import { SessionContext } from '@assemblage/components'

import { NotepadContext } from './notepad'
import { readLines, readTitle, readParticipation } from './readers'
import ParticipantList from './participants'
import Line from './line'
import ActionsMenu from './actions-menu'

function readState (opts) {
  return {
    title: readTitle(opts),
    lines: readLines(opts),
    participation: readParticipation(opts)
  }
}

/**
 * Prototype pad editor.
 *
 * Note that this implementation is a prototype.
 * The state logic should be replaced by
 * an improved version that supports immediate UI updates
 * (i.e. not waiting on new line), conflict resolution
 * and clean unmount life cycle handling.
 */
export default function PadEditor () {
  const { doc, entity, store, updater, subjectUrl } = React.useContext(NotepadContext)
  const { session } = React.useContext(SessionContext)
  const [state, setState] = React.useState(readState({ store, subjectUrl }))
  const [mode, setMode] = React.useState('view')

  const { title, lines, participation, focus } = state

  const update = (updatedLines, focus) => {
    setState({
      title,
      participation: readParticipation({ store, subjectUrl }),
      focus,
      lines: updatedLines ||
        readLines({ store, subjectUrl })
    })
  }

  React.useEffect(() => {
    console.log('Registering change listener')
    updater.addDownstreamChangeListener(doc, () => {
      console.log('Change on PadEditor')
      update()
    })
    // TODO remove stream change listener on clean up
  }, [doc, store, subjectUrl, updater])

  const toggleMode = () => {
    setMode(mode === 'edit' ? 'view' : 'edit')
    // TODO handle if updating?
    update()
  }

  if (mode === 'edit' && !session.isLoggedIn) {
    setMode('view')
  }

  return (
    <Box display='flex' mb={4}>
      <Box flexGrow={1} px={3} py={2}>
        <Box display='flex'>
          <Typography variant='h5'>{title}</Typography>
          {session.isLoggedIn &&
            <Box style={{ marginLeft: 'auto' }}>
              <IconButton onClick={toggleMode}>
                {mode === 'edit' ? <ViewIcon />
                  : <Tooltip title='edit pad'><EditIcon /></Tooltip>}
              </IconButton>
              <ActionsMenu />
            </Box>}
        </Box>
        {lines.map((line, i) => {
          const now = new Date()
          return (
            <Line
              key={line.subject + now.getTime()}
              mode={mode}
              value={line}
              participation={participation}
              update={update}
              setMode={setMode}
              focus={i === focus}
            />)
        })}
        {lines.length === 0 &&
          <Line
            mode='new'
            participation={participation}
            update={update}
          />}
      </Box>
      <Divider orientation='vertical' flexItem light />
      <ParticipantList participations={participation.data} />
    </Box>)
}
