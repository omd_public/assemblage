import React from 'react'

import { debounce } from 'throttle-debounce'

import {
  Box,
  InputBase,
  InputAdornment,
  Typography,
  Slide,
  Snackbar,
  Menu,
  MenuItem,
  makeStyles
} from '@material-ui/core'
import {
  Check as UpdatedIcon,
  Update as UpdatingIcon
} from '@material-ui/icons'

import { SessionContext } from '@assemblage/components'

import { NotepadContext } from './notepad'
import { readLines } from './readers'
import {
  newLineStatements,
  updateContentStatements,
  newParticipationStatements,
  deleteLineStatements
} from './writers'

const RETURN_CODE = 13
const DELETE_CODE = 8
const ESCAPE_CODE = 27

const DEBOUNCE_MILLIS = 500

const useBulletStyles = makeStyles(theme => ({
  bullet: {
    borderRadius: '50%',
    width: '0.5rem',
    height: '0.5rem',
    minHeight: '0.5rem',
    minWidth: '0.5rem',
    maxHeight: '0.5rem',
    maxWidth: '0.5rem',
    backgroundColor: 'black',
    display: 'inline-block',
    marginRight: '1rem',
    '&:hover': {
      cursor: 'pointer',
      boxShadow: '0 0 0 10px #DDDDDD'
    }
  }
}))

function SlideTransition (props) {
  return <Slide {...props} direction='up' />
}

function Bullet ({ indent, mode }) {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const classes = useBulletStyles()
  const indentLevel = `${1 + parseInt(indent || 0)}rem`

  const handleClick = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <span className={classes.bullet} style={{ marginLeft: indentLevel }} onClick={handleClick} />
      {mode === 'edit' &&
        <Menu
          id='bullet-menu'
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>Increase Indent</MenuItem>
          <MenuItem onClick={handleClose}>Decrease Indent</MenuItem>
        </Menu>}
    </>
  )
}

// TODO handle add participation...
// TODO allow user to set color?
// TODO think on where to add the indent level to be able to collapse expand by
// indent level
export default function Line (props) {
  const { update, participation, mode, setMode, focus } = props
  const [currentLine, setCurrentLine] = React.useState(props.value)
  const [updateControl, setUpdateControl] = React.useState({
    dirty: false
  })
  const [error, setError] = React.useState({
    open: false
  })
  const { store, updater, subjectUrl, doc } = React.useContext(NotepadContext)
  const { session } = React.useContext(SessionContext)
  const me = session.currentSession && session.currentSession.webId

  const updateGraph = (del, ins, onSuccess, onError) => {
    updater.update(del, ins, (uri, ok, message) => {
      if (ok) {
        onSuccess()
      } else if (onError) {
        onError()
      } else {
        setError({
          open: true,
          message
        })
      }
    })
  }

  const createNewLine = (opts = {
    store,
    doc,
    me,
    currentLine
  }) => {
    const { del, newLine, ...nlStms } = newLineStatements(opts)
    let { ins } = nlStms

    if (participation.map[me] === undefined) {
      const stms = newParticipationStatements({ subjectUrl, doc, me })
      ins = ins.concat(stms.ins)
    }

    updateGraph(del, ins, () => {
      const lines = readLines({ store, subjectUrl })
      const index = lines.findIndex(l => {
        return l.subject.value === newLine.value
      })

      update(lines, index)
    })
  }

  // On empty document
  if (mode === 'new') {
    createNewLine({
      store,
      currentLine: {
        subject: subjectUrl
      },
      me,
      doc
    })
    return null
  }

  const updateContent = (newValue) => {
    const { ins, del } = updateContentStatements({
      doc, currentLine, newValue
    })

    updateGraph(del, ins, () => {
      setUpdateControl({
        dirty: false,
        success: true
      })
    }, () => {
      // On error reset all lines
      // not very kind but does the job
      update()
    })
  }

  const updateContentDebounced = React.useCallback(
    debounce(DEBOUNCE_MILLIS, updateContent),
    [updateControl.dirty]
  )

  // XXX not very clean, still can be in flight updates
  React.useEffect(() => updateContentDebounced.cancel, [])

  const handleChange = event => {
    const newValue = event.target.value

    setUpdateControl({
      dirty: true
    })

    setCurrentLine({
      ...currentLine,
      content: newValue
    })

    updateContentDebounced(newValue)
  }

  const handleKeyDown = event => {
    if (updateControl.dirty) {
      return
    }

    if (event.keyCode === RETURN_CODE) {
      createNewLine()
    } else if (event.keyCode === DELETE_CODE) {
      if (!currentLine.content) {
        const { ins, del } = deleteLineStatements({ store, doc, currentLine })
        if (ins && del) {
          updateGraph(del, ins, update)
        }
      }
    } else if (event.keyCode === ESCAPE_CODE) {
      setMode('view')
      update()
    }
  }

  const participant = participation.map[currentLine.author]

  if (mode === 'edit') {
    return (
      <Box
        my={1}
        display='flex'
        alignItems='center'
      >
        <Bullet indent={currentLine.indent} mode={mode} />
        <InputBase
          fullWidth
          value={currentLine.content}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          inputRef={i => {
            i && focus && i.focus()
          }}
          inputProps={{
            style: {
              borderBottom: '1px dotted',
              borderColor: participant ? participant.bgColor : '#000000'
            }
          }}
          endAdornment={
            <InputAdornment position='end'>
              {updateControl.success
                ? <UpdatedIcon />
                : updateControl.dirty
                  ? <UpdatingIcon /> : <span />}
            </InputAdornment>
          }
        />
        <Snackbar
          open={error.open}
          onClose={() => {
            setError({ open: false })
          }}
          TransitionComponent={SlideTransition}
          message={error.message}
        />
      </Box>
    )
  }

  return (
    <Box
      my={1}
      display='flex'
      alignItems='center'
    >
      <Bullet indent={currentLine.indent} />
      <Typography>
        {currentLine.content}
      </Typography>
    </Box>
  )
}
