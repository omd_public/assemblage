import React from 'react'

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
  Box,
  Slide
} from '@material-ui/core'
import { RootContext, SessionContext } from '@assemblage/components'
import { asFolderPath } from '@assemblage/location'

import { createNewNotepad } from './writers'

const Transition = React.forwardRef(function Transition (props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

export default function ({ open, onClose, onSuccess, onError, beforeCreate }) {
  const { currentLocation, navigateTo } = React.useContext(RootContext)
  const { session } = React.useContext(SessionContext)
  const defaultFolder = asFolderPath(currentLocation.url)
  const [state, setState] = React.useState({
    folder: defaultFolder
  })

  const handleChange = prop => event => {
    setState({ ...state, [prop]: event.target.value })
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    const { title, folder } = state
    const me = session.currentSession && session.currentSession.webId

    beforeCreate && beforeCreate()

    createNewNotepad({
      folder,
      title,
      me,
      onSuccess: (docUri) => {
        onClose()
        onSuccess && onSuccess()
        navigateTo({ url: docUri })
      },
      onError: error => {
        console.error(error)
        onError && onError()
      }
    })
  }

  return (
    <Dialog
      open={open}
      onClose={onClose}
      TransitionComponent={Transition}
    >
      <form onSubmit={handleSubmit}>
        <DialogTitle id='notepad-dialog-title'>New Notepad</DialogTitle>
        <DialogContent dividers>
          <Box mb={2}>
            <TextField
              required
              id='title'
              label='title'
              onChange={handleChange('title')}
            />
          </Box>
          <Box mb={2}>
            <TextField
              type='url'
              required
              id='folder'
              label='folder'
              value={state.folder}
              onChange={handleChange('folder')}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>
            Cancel
          </Button>
          <Button type='submit' color='primary'>
            Create
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}
