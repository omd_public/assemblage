import React from 'react'

import { LaunchButton } from '@assemblage/components'

import notepadIcon from './images/icon.svg'
import NotepadCreator from './creator'

export default function (props) {
  const [state, setState] = React.useState({
    open: false
  })

  return (
    <>
      <LaunchButton
        label='Notepad'
        onClick={() => setState({ open: true })}
        src={notepadIcon}
      />
      <NotepadCreator
        open={state.open}
        onClose={() => setState({ open: false })}
        {...props}
      />
    </>
  )
}
