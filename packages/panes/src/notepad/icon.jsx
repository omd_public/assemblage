import React from 'react'

import icon from './images/icon.svg'

export default function (props) {
  return <img src={icon} {...props} />
}
