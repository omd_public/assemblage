import React from 'react'

import {
  Box,
  List,
  Button,
  Divider,
  Typography
} from '@material-ui/core'
import { appendPath } from '@assemblage/utils'
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'
import {
  CreateNewFolder as CreateNewFolderIcon,
  AddCircle as CreateNewResourceIcon,
  Add,
  ViewComfy,
  ViewList
} from '@material-ui/icons'
import { RootContext, SessionContext, Breadcrumbs, fileClient } from '@assemblage/components'

import CreateSpaceDialog from './resource-create-space-dialog'
import CreateResourceDialog from './resource-create-dialog'
import ResourceListItem from './resource-list-item'
import { PaneBox } from '../ui'

const useStyles = makeStyles(theme => ({
  divider: {
    alignSelf: 'stretch',
    height: 'auto',
    margin: theme.spacing(1, 0.75)
  },
  pushRight: {
    marginLeft: 'auto'
  },
  breadcrumbs: {
    marginBottom: theme.spacing(1)
  }
}))

function ResourceList ({ resources = [], empty }) {
  if (resources.length === 0) {
    return empty
  }

  const length = resources.length - 1

  return (
    <List>
      {resources.map((r, i) => {
        return (
          <div key={i}>
            <ResourceListItem {...r} />
            {i < length && <Divider variant='inset' />}
          </div>
        )
      })}
    </List>
  )
}

function Empty ({ onClick }) {
  const { session } = React.useContext(SessionContext)

  return (
    <Box
      py={3}
      display='flex'
      borderRadius={2}
      flexDirection='column'
      justifyContent='center'
      alignItems='center'
    >
      <Typography color='textSecondary'>No items here.</Typography>
      {session.isOwner &&
        <Box mt={3}>
          <Button variant='outlined' size='large' onClick={onClick}>
            <Add /> Add
          </Button>
        </Box>}
    </Box>
  )
}

function Toolbar ({ onClick, icon }) {
  const { session } = React.useContext(SessionContext)
  const classes = useStyles()

  return (
    <Box display='flex' flexWrap='nowrap' className={classes.pushRight}>
      <ToggleButtonGroup
        exclusive
        value='list'
        size='small'
        aria-label='view mode'
      >
        <ToggleButton value='list'>
          <ViewList />
        </ToggleButton>
        <ToggleButton value='grid'>
          <ViewComfy />
        </ToggleButton>
      </ToggleButtonGroup>
      {session.isOwner &&
        <>
          <Divider orientation='vertical' className={classes.divider} />
          <Button onClick={onClick} startIcon={icon}>New</Button>
        </>}
    </Box>
  )
}

function ResourceExplorer ({ subjectUrl, containers, resources }) {
  const classes = useStyles()
  const [createSpaceState, setCreateSpaceState] = React.useState({
    open: false
  })
  const [createResourceState, setCreateResourceState] = React.useState({
    open: false
  })
  const { reload } = React.useContext(RootContext)

  const closeCreateSpace = () => {
    setCreateSpaceState({
      open: false
    })
  }
  const createSpace = event => {
    event.preventDefault()

    setCreateSpaceState({
      open: true,
      onClose: closeCreateSpace,
      onSubmit: async (state) => {
        try {
          const newUrl = appendPath(subjectUrl, state.name)
          const res = await fileClient.createFolder(newUrl)
          if (res.ok) {
            closeCreateSpace()
            reload('browser')
          } else {
            console.log(res)
          }
        } catch (error) {
          closeCreateSpace()
          console.error(error)
        }
      }
    })
  }

  const closeCreateResource = () => {
    setCreateResourceState({
      open: false
    })
  }
  const createResource = event => {
    event.preventDefault()

    setCreateResourceState({
      open: true,
      onClose: closeCreateResource,
      onSubmit: async (state) => {
        console.log('lol')
      }
    })
  }

  return (
    <Box mb={5}>
      <Breadcrumbs
        className={classes.breadcrumbs}
        subjectUrl={subjectUrl}
        maxItems={5}
        view='browser'
        aria-label='breadcrumbs'
      />
      <PaneBox
        title='Spaces'
        appbar={<Toolbar onClick={createSpace} icon={<CreateNewFolderIcon />} />}
      >
        {createSpaceState.open &&
          <CreateSpaceDialog {...createSpaceState} />}
        <ResourceList
          resources={containers}
          empty={<Empty onClick={createSpace} />}
        />
      </PaneBox>

      <PaneBox
        title='Resources'
        appbar={<Toolbar onClick={createResource} icon={<CreateNewResourceIcon />} />}
      >
        {createResourceState.open &&
          <CreateResourceDialog subjectUrl={subjectUrl} {...createResourceState} />}
        <ResourceList
          resources={resources}
          empty={<Empty onClick={createResource} />}
        />
      </PaneBox>
    </Box>
  )
}

export default ResourceExplorer
