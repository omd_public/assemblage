import React from 'react'

import { Controlled as CodeMirror } from 'react-codemirror'
import 'codemirror/config'

import {
  Image as ImageIcon,
  Description as TextIcon
} from '@material-ui/icons'
import {
  Slide,
  Button,
  Box,
  TextField,
  Select,
  MenuItem,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  makeStyles
} from '@material-ui/core'

import { RootContext } from '@assemblage/components'
import { Registry } from '@assemblage/registry'
import { slugify } from '@assemblage/utils'

import { withFetch, saveFile } from './resource-manager'

// TODO extract some components :P

const useStyles = makeStyles(theme => ({
  editor: {
    '& .CodeMirror': {
      height: 'auto'
    }
  }
}))

const Transition = React.forwardRef(function Transition (props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

const ImageDisplay = withFetch(({ data }) => {
  return <Box m={2} display='flex' justifyContent='center'><img src={URL.createObjectURL(data)} /></Box>
})

const fileTypes = [
  { label: 'Turtle', contentType: 'text/turtle', extension: '.ttl' },
  { label: 'Plain Text', contentType: 'text/plain', extension: '.txt' }
]

function SourceCreate ({ subjectUrl, open, onClose, beforeCreate }) {
  const [state, setState] = React.useState({
    contentType: 'text/turtle',
    folder: subjectUrl,
    fileName: ''
  })
  const { navigateTo } = React.useContext(RootContext)

  const handleChange = prop => event => {
    setState({ ...state, [prop]: event.target.value })
  }

  const createFile = async event => {
    event.preventDefault()

    const { folder, fileName, contentType } = state
    // TODO make this more robust
    const docUri = folder +
      slugify(fileName) +
      fileTypes.find(ft => ft.contentType === contentType).extension

    beforeCreate && beforeCreate()

    await saveFile({
      subjectUrl: docUri,
      value: '',
      contentType
    })

    onClose()
    navigateTo({ url: docUri })
  }

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby='add-resource-dialog-title'
      TransitionComponent={Transition}
    >
      <DialogTitle id='add-resource-dialog-title'>Source File</DialogTitle>
      <form onSubmit={createFile}>
        <DialogContent dividers>
          <Box mb={2}>
            <Select onChange={handleChange('contentType')} value={state.contentType}>
              {fileTypes.map((f, i) => (<MenuItem key={i} value={f.contentType}>{f.label}</MenuItem>))}
            </Select>
          </Box>
          <Box mb={2}>
            <TextField
              id='file-name'
              label='file name'
              required
              value={state.fileName}
              onChange={handleChange('fileName')}
              placeholder='file name...'
            />
          </Box>
          <Box mb={2}>
            <TextField
              id='folder'
              label='folder'
              required
              type='url'
              value={state.folder}
              onChange={handleChange('folder')}
              placeholder='destination folder...'
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>
            Cancel
          </Button>
          <Button type='submit' color='primary'>
            Create
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

const SourceDisplay = withFetch(({ subjectUrl, headers, data, mode }) => {
  const [state, setState] = React.useState({ value: data, saved: true })
  const contentType = headers.get('Content-Type')
  const classes = useStyles()

  const handleSave = () => {
    saveFile({
      subjectUrl,
      value: state.value,
      contentType
    })
    setState({
      value: state.value,
      saved: true
    })
  }

  const options = {
    mode: contentType,
    theme: 'material',
    lineNumbers: true,
    readOnly: mode === 'view',
    styleActiveLine: true,
    matchBrackets: true
  }

  return (
    <>
      <CodeMirror
        className={classes.editor}
        value={state.value}
        options={options}
        onBeforeChange={(editor, data, value) => {
          setState({ value, saved: false })
        }}
      />
      {mode === 'edit' &&
        <Box my={1} display='flex' justifyContent='flex-end'>
          <Button disabled={state.saved}>
            Cancel
          </Button>
          <Button color='primary' disabled={state.saved} onClick={handleSave}>
            Save
          </Button>
        </Box>}
    </>
  )
})

function includesWithRegExp (regExpArray, toMatch) {
  for (let i = 0; i < regExpArray.length; i++) {
    const regExp = regExpArray[i]
    if (regExp instanceof RegExp &&
      regExp.test(toMatch)) {
      return true
    } else if (regExp === toMatch) {
      return true
    }
  }
  return false
}

const catalog = [
  {
    types: [
      'http://purl.org/dc/terms/Image',
      'http://www.w3.org/ns/iana/media-types/image/png#Resource',
      'http://www.w3.org/ns/iana/media-types/image/vnd.microsoft.icon#Resource'
    ],
    label: 'Image',
    icon: <ImageIcon fontSize='inherit' />,
    display: (props) => <ImageDisplay {...props} />
  },
  {
    types: [
      /.*/
    ],
    label: 'Source',
    icon: <TextIcon fontSize='inherit' />,
    display: (props) => <SourceDisplay {...props} />,
    create: (props) => <SourceCreate {...props} />
  }
]

class ResourceCatalog {
  static getDisplaysFor (types) {
    const typesArray = [].concat(types)
    const components = catalog.filter(c =>
      typesArray.some(
        t => c.types && includesWithRegExp(c.types, t)
      )
    )
    return components
  }

  static getAllCreators () {
    const builtIns = catalog.filter(c => c.create !== undefined)
    const remotes = Registry.getCreatorPanes('browser')
    return builtIns.concat(remotes.map(p => {
      const Icon = p.iconComponent
      return {
        label: p.label,
        create: p.creatorComponent,
        remote: true,
        icon: <Icon style={{ width: '28px', marginLeft: '5px', filter: 'invert(1)' }} />
      }
    }))
  }
}

export default ResourceCatalog
