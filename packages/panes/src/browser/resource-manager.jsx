import React from 'react'

import {
  Box
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { fileClient, readFile, ErrorMessage } from '@assemblage/components'

export async function saveFile ({ subjectUrl, value, contentType }) {
  try {
    const res = await fileClient.putFile(subjectUrl, value, contentType)
    console.log(res)
  } catch (error) {
    console.error(error)
  }
}

export function withFetch (Component) {
  return function componentWithFetch ({ subjectUrl, ...props }) {
    const [state, setState] = React.useState({
      loaded: false,
      data: null
    })

    React.useEffect(() => {
      let mounted = true

      readFile(subjectUrl).then(({ data, headers }) => {
        if (mounted) {
          setState({
            loaded: true,
            headers,
            data
          })
        }
      }).catch(error => {
        console.error(error)

        if (mounted) {
          setState({
            error
          })
        }
      })

      return () => {
        mounted = false
      }
    }, [subjectUrl])

    if (state.loaded) {
      return (
        <Component
          data={state.data}
          headers={state.headers}
          subjectUrl={subjectUrl}
          {...props}
        />)
    } else if (state.error) {
      return <ErrorMessage error={state.error} />
    } else {
      return <Box m={2}><Skeleton height={60} /></Box>
    }
  }
}
