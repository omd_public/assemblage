import React from 'react'

import { RootContext, SessionContext, LaunchButton } from '@assemblage/components'

import icon from './images/icon.svg'

export default function ({ onSuccess }) {
  const { navigateTo } = React.useContext(RootContext)
  const { session } = React.useContext(SessionContext)

  if (!session.isLoggedIn) {
    return null
  }

  const handleClick = () => {
    onSuccess && onSuccess()

    const { account } = session
    navigateTo({
      url: account.storage,
      view: 'browser'
    })
  }

  return (
    <LaunchButton
      label='Storage'
      onClick={handleClick}
      src={icon}
    />
  )
}
