import React from 'react'

import { useReify } from '@assemblage/hooks'
import { withLoader } from '@assemblage/components'
import { basicInfo, rdfTypes, each, expand } from '@assemblage/ld'

import ResourceExplorer from './resource-explorer'

function Browser ({ it, subjectUrl }) {
  const containers = []
  const resources = []
  const { children } = it
  const ldpContainer = expand('ldp:Container')

  if (children) {
    for (let i = 0; i < children.length; i++) {
      const child = children[i]
      const { value } = child
      if (value.types.includes(ldpContainer)) {
        child.value.isContainer = true
        containers.push(child)
      } else {
        resources.push(child)
      }
    }
  }

  return (
    <ResourceExplorer
      subjectUrl={subjectUrl}
      containers={containers}
      resources={resources}
    />
  )
}

export default function (props) {
  const state = useReify({
    info: basicInfo,
    children: each('ldp:contains', (graphOpts) => {
      return {
        info: basicInfo(graphOpts),
        types: rdfTypes(graphOpts)
      }
    })
  }, props)

  const LoadBrowser = withLoader(Browser)

  return (
    <LoadBrowser
      state={state}
      subjectUrl={props.subjectUrl}
      placeholder={null}
    />
  )
}
