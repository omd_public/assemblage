const modes = {
  read: 'http://www.w3.org/ns/auth/acl#Read',
  write: 'http://www.w3.org/ns/auth/acl#Write',
  control: 'http://www.w3.org/ns/auth/acl#Control',
  append: 'http://www.w3.org/ns/auth/acl#Append'
}

function areArraysEqualSets (a1, a2) {
  const superSet = {}
  for (let i = 0; i < a1.length; i++) {
    const e = a1[i] + typeof a1[i]
    superSet[e] = 1
  }

  for (let i = 0; i < a2.length; i++) {
    const e = a2[i] + typeof a2[i]
    if (!superSet[e]) {
      return false
    }
    superSet[e] = 2
  }

  for (const e in superSet) {
    if (superSet[e] === 1) {
      return false
    }
  }

  return true
}

const accessLevels = {
  owners: {
    id: 'ControlReadWrite',
    role: 'Owner',
    modes: [
      modes.read,
      modes.write,
      modes.control
    ]
  },
  editors: {
    id: 'ReadWrite',
    role: 'Editor',
    modes: [
      modes.read,
      modes.write
    ]
  },
  posters: {
    id: 'AppendRead',
    role: 'Poster',
    modes: [
      modes.append,
      modes.read
    ]
  },
  submitters: {
    id: 'Append',
    role: 'Submitter',
    modes: [
      modes.append
    ]
  },
  viewers: {
    id: 'Read',
    role: 'Viewer',
    modes: [
      modes.read
    ]
  }
}

export function getAccessLevelModesByRole (role) {
  for (const k in accessLevels) {
    const level = accessLevels[k]
    if (level.role.toLowerCase() === role) {
      return level.modes
    }
  }
  return []
}

export function mapAccessLevels (subjectUrl, acl) {
  const res = []

  for (const k in accessLevels) {
    const level = accessLevels[k]
    const uri = `${subjectUrl}.acl#${level.id}`
    const filtered = []

    for (const a in acl) {
      const entry = acl[a]
      const modes = [].concat(entry.modes)
      if (areArraysEqualSets(level.modes, modes)) {
        filtered.push(entry)
      }
    }

    res.push({
      ...level,
      uri,
      list: filtered
    })
  }

  return res
}

export function findSubjectForModes (acl, modes) {
  for (const a in acl) {
    const entry = acl[a]
    const entryModes = [].concat(entry.modes)
    if (areArraysEqualSets(modes, entryModes)) {
      return {
        uri: a,
        exists: true
      }
    }
  }
  for (const l in accessLevels) {
    const entry = accessLevels[l]
    if (areArraysEqualSets(modes, entry.modes)) {
      return {
        entry,
        exists: false
      }
    }
  }
  return null
}
