import React from 'react'

import {
  Box,
  Typography,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  ListItemAvatar,
  Avatar,
  CircularProgress
} from '@material-ui/core'
import {
  Delete as DeleteIcon,
  Public as PublicIcon,
  VerifiedUser as AuthUserIcon
} from '@material-ui/icons'

import { AgentDetails } from '../../ui'

const AgentClassMap = {
  'http://xmlns.com/foaf/0.1/Agent': {
    label: 'Everyone',
    icon: <PublicIcon />
  },
  'http://www.w3.org/ns/auth/acl#AuthenticatedAgent': {
    label: 'Authenticated Users',
    icon: <AuthUserIcon />
  }
}

function mapProfile (list) {
  return [].concat(list).map(a => {
    const resolved = AgentClassMap[a]
    if (resolved) {
      return {
        ...resolved,
        found: true,
        id: a
      }
    }

    return {
      id: a,
      found: false
    }
  })
}

function concatProfiles (arr) {
  let res = []
  arr.forEach(a => {
    if (a) {
      res = res.concat(mapProfile(a))
    }
  })
  return res
}

function ItemActions ({ agent, handleDelete }) {
  const [state, setState] = React.useState({
    loading: false
  })

  const onClick = event => {
    event.preventDefault()
    event.stopPropagation()

    setState({
      loading: true
    })

    handleDelete(agent)
  }

  return (
    <ListItemSecondaryAction>
      {state.loading ? <CircularProgress size={24} />
        : <IconButton onClick={onClick}>
          <DeleteIcon />
          </IconButton>}
    </ListItemSecondaryAction>
  )
}

export default function AccessList ({ store, level, handleDelete }) {
  const { list } = level
  let agents = []

  list.forEach(it => {
    agents = concatProfiles([
      it.agentClass, it.agent, it.agentGroup, it.origin, it.originClass
    ])
  })

  if (agents.length === 0) {
    return null
  }

  return (
    <Box mt={1}>
      <Typography variant='h6'>{level.role}</Typography>
      <List>
        {agents.map((a, i) => (
          a.found
            ? <ListItem key={a.id}>
              <ListItemAvatar>
                <Avatar alt={a.label}>
                  {a.icon && a.icon}
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={a.label} />
              <ItemActions agent={a.id} handleDelete={handleDelete} />
            </ListItem>
            : <AgentDetails key={a.id} subjectUrl={a.id} store={store}>
              <ItemActions agent={a.id} handleDelete={handleDelete} />
              </AgentDetails>
        ))}
      </List>
    </Box>
  )
}
