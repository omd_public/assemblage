import React from 'react'

import {
  Box,
  Paper,
  Divider,
  Typography,
  TextField,
  Select,
  Button,
  MenuItem,
  FormControl,
  InputLabel,
  CircularProgress,
  makeStyles
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'

import { reify } from '@assemblage/hooks'
import { aclInfo } from '@assemblage/ld'
import { fileClient } from '@assemblage/components'

import {
  mapAccessLevels,
  findSubjectForModes,
  getAccessLevelModesByRole
} from './access-levels'
import { updateAcl, deleteAgent } from './writers'
import AccessList from './access-list'

const useStyles = makeStyles(theme => ({
  addInput: {
    marginRight: theme.spacing(2)
  }
}))

function Acl ({ subjectUrl }) {
  let mounted = true

  const classes = useStyles()
  const [aclState, setAclState] = React.useState({
    loading: true
  })
  const [state, setState] = React.useState({
    agentType: 'everyone',
    agentUri: '',
    role: 'viewer',
    submitted: false
  })

  const loadAcl = async () => {
    try {
      const links = await fileClient.getItemLinks(subjectUrl)

      if (await fileClient.itemExists(links.acl)) {
        const ent = await reify({
          info: aclInfo
        }, {
          subjectUrl: links.acl
        })
        if (mounted) {
          setAclState({
            loading: false,
            acl: ent.info,
            store: ent.store,
            updater: ent.updater
          })
        }
      } else if (mounted) {
        // Item does not exist
        // TODO get from parent etc?
        setAclState({
          loading: false,
          notFound: true
        })
      }
    } catch (error) {
      console.error(error)
      if (mounted) {
        setAclState({
          loading: false,
          error
        })
      }
    }
  }

  React.useEffect(() => {
    loadAcl()
    return () => {
      mounted = false
    }
  }, [subjectUrl])

  if (aclState.loading) {
    return <Paper><Box p={2} mt={2}><Skeleton height={100} /></Box></Paper>
  }

  if (aclState.notFound) {
    return <div>Not FOUND</div>
  }

  if (aclState.error) {
    return <div>Error</div>
  }

  const { acl, store, updater } = aclState
  const levels = mapAccessLevels(subjectUrl, acl)

  // The store was initially loaded by reify
  const reloadAcl = (callback) => {
    const acl = aclInfo({
      store,
      subjectUrl
    })
    setAclState({
      loading: false,
      acl,
      store,
      updater
    })
    callback && callback()
  }

  const handleChange = prop => event => {
    setState({ ...state, [prop]: event.target.value })
  }

  const onSubmit = state => {
    const modes = getAccessLevelModesByRole(state.role)
    const levelSubjectOpts = findSubjectForModes(acl, modes)
    const statements = updateAcl({
      store,
      subjectUrl,
      state,
      levelSubjectOpts
    })

    update(statements)
  }

  const update = ({ del, ins }) => {
    updater.update(del, ins, (uri, ok, message) => {
      console.log(message, uri)
      if (ok) {
        reloadAcl(() => {
          setState({ ...state, submitted: false })
        })
      } else {
        console.error(message)
        // TODO handle errors
        /* setError({
          open: true,
          message
        }) */
      }
    })
  }

  const handleDelete = agent => {
    const statements = deleteAgent({
      subjectUrl,
      store,
      agent
    })
    update(statements)
  }

  const handleSubmit = event => {
    event.preventDefault()
    event.stopPropagation()

    if (!state.submitted) {
      setState({ ...state, submitted: true })
      onSubmit(state)
    }
  }

  const needUri = [
    'person', 'group', 'origin'
  ]

  return (
    <Paper>
      <Box p={2} mt={2}>
        <Box mb={4}>
          <Typography variant='h5'>Add Agent to Role</Typography>
          <form onSubmit={handleSubmit}>
            <Box display='flex' alignItems='flex-end' mt={2}>
              <FormControl>
                <InputLabel id='agent-type-label'>Agent</InputLabel>
                <Select
                  labelId='agent-type-label'
                  required
                  autoWidth
                  value={state.agentType}
                  onChange={handleChange('agentType')}
                  className={classes.addInput}
                >
                  <MenuItem value='everyone'>Everyone</MenuItem>
                  <MenuItem value='authenticatedAgent'>Authenticated Users</MenuItem>
                  <MenuItem value='person'>Person</MenuItem>
                  <MenuItem value='group'>Group</MenuItem>
                  <MenuItem value='origin'>Application</MenuItem>
                </Select>
              </FormControl>
              {needUri.includes(state.agentType) &&
                <TextField
                  required
                  label='URI'
                  value={state.agentUri}
                  onChange={handleChange('agentUri')}
                  placeholder='enter URI...'
                  type='url'
                  className={classes.addInput}
                />}
              <FormControl>
                <InputLabel id='agent-role-label'>Role</InputLabel>
                <Select
                  labelId='agent-role-label'
                  autoWidth
                  required
                  value={state.role}
                  onChange={handleChange('role')}
                  className={classes.addInput}
                >
                  <MenuItem value='owner'>Owner</MenuItem>
                  <MenuItem value='editor'>Editor</MenuItem>
                  <MenuItem value='viewer'>Viewer</MenuItem>
                  <MenuItem value='submitter'>Submitter</MenuItem>
                  <MenuItem value='poster'>Poster</MenuItem>
                </Select>
              </FormControl>
              {state.submitted
                ? <CircularProgress size={24} />
                : <Button type='submit'>
                    Add
                  </Button>}
            </Box>
          </form>
        </Box>
        <Divider />
        <Box mt={2}>
          <Typography variant='h5'>Access Roles</Typography>
          {levels.map((level, i) => (
            <AccessList key={i} level={level} store={store} handleDelete={handleDelete} />
          ))}
        </Box>
      </Box>
    </Paper>
  )
}

export default Acl
