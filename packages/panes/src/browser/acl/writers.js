import * as $rdf from 'rdflib'

import { slugify } from '@assemblage/utils'
import { createUpdater, anyObject, anySubject, ns } from '@assemblage/ld'

const typesMap = {
  everyone: {
    predicate: 'acl:agentClass',
    object: 'http://xmlns.com/foaf/0.1/Agent'
  },
  authenticatedAgent: {
    predicate: 'acl:agentClass',
    object: 'http://www.w3.org/ns/auth/acl#AuthenticatedAgent'
  },
  person: {
    predicate: 'acl:agent'
  },
  group: {
    predicate: 'acl:agentGroup'
  },
  origin: {
    predicate: 'acl:origin'
  }
}

export function deleteAgent ({
  subjectUrl,
  store,
  agent
}) {
  const doc = store.sym(subjectUrl + '.acl').doc()
  const agentNode = $rdf.sym(agent)
  const del = store.match(undefined, undefined, agentNode, doc)

  return {
    ins: [],
    del
  }
}

export function updateAcl ({
  subjectUrl,
  store,
  state,
  levelSubjectOpts
}) {
  const doc = store.sym(subjectUrl + '.acl').doc()
  const ins = []

  let aclSubj
  let del = []

  if (levelSubjectOpts.exists) {
    aclSubj = $rdf.sym(levelSubjectOpts.uri)
  } else {
    const { entry } = levelSubjectOpts
    const thisSubj = $rdf.sym(subjectUrl + './')

    aclSubj = $rdf.sym(`${subjectUrl}.acl#${entry.id}`)

    ins.push($rdf.st(aclSubj, ns('rdf:type'), ns('acl:Authorization'), doc))
    ins.push($rdf.st(aclSubj, ns('acl:accessTo'), thisSubj, doc))
    ins.push($rdf.st(aclSubj, ns('acl:default'), thisSubj, doc))

    entry.modes.forEach(m => {
      ins.push($rdf.st(aclSubj, ns('acl:mode'), $rdf.sym(m), doc))
    })
  }
  const { agentType, agentUri } = state
  const expandedType = typesMap[agentType]
  const typeObject = $rdf.sym(expandedType.object || agentUri)

  del = store.match(undefined, undefined, typeObject, doc)
  ins.push($rdf.st(aclSubj, ns(expandedType.predicate), typeObject, doc))

  return {
    del,
    ins
  }
}
