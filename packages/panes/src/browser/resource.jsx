import React from 'react'

import {
  Typography,
  Box,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from '@material-ui/core'
import {
  ExpandMore
} from '@material-ui/icons'
import { useReify } from '@assemblage/hooks'
import { Breadcrumbs, withLoader, SessionContext } from '@assemblage/components'
import { basicInfo, rdfTypes } from '@assemblage/ld'
import { toLabel } from '@assemblage/utils'

import ResourceDisplay from './resource-display'

export function Resource ({ it, subjectUrl }) {
  const { session } = React.useContext(SessionContext)

  const { types, info } = it
  const label = toLabel(subjectUrl)
  const mode = session.isLoggedIn ? 'edit' : 'view'

  return (
    <Box my={2}>
      <ExpansionPanel defaultExpanded>
        <ExpansionPanelSummary
          expandIcon={<ExpandMore />}
        >
        Source
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{ padding: '0.5rem 0' }}>
          <Box width='100%'>
            <ResourceDisplay
              label={label}
              subjectUrl={subjectUrl}
              types={types}
              mode={mode}
            />
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMore />}
        >
        Resource Details
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Box display='flex' flexDirection='column'>

            <Typography>Name: {label}</Typography>
            <Typography>IRI: {subjectUrl}</Typography>
            {info &&
              <>
                <Typography>Modified: {info.modified}</Typography>
                <Typography>Size: {info.size} bytes</Typography>
              </>}
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </Box>
  )
}

function ResourceLoader (props) {
  const state = useReify({
    info: basicInfo,
    types: rdfTypes
  }, props)
  const LoadResource = withLoader(Resource)
  const { subjectUrl } = props

  return (
    <>
      <Breadcrumbs
        subjectUrl={subjectUrl}
        maxItems={5}
        aria-label='breadcrumb'
        view='browser'
      />

      <LoadResource
        state={state}
        subjectUrl={subjectUrl}
      />
    </>
  )
}

export default ResourceLoader
