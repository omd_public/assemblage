import React from 'react'

import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogContentText,
  DialogActions,
  TextField,
  Button,
  Box,
  Typography,
  CircularProgress
} from '@material-ui/core'

export default function CreateDialog ({ open, onClose, onSubmit }) {
  const [state, setState] = React.useState({
    name: '',
    submitted: false
  })

  const handleChange = prop => event => {
    setState({ ...state, [prop]: event.target.value })
  }

  const handleSubmit = event => {
    event.preventDefault()

    if (!state.submitted) {
      setState({ ...state, submitted: true })
      onSubmit(state)
    }
  }

  if (state.submitted) {
    return (
      <Dialog open={open} onClose={onClose}>
        <DialogTitle id='form-create'>Create Space</DialogTitle>
        <DialogContent>
          <Box
            p={2}
            display='flex'
            flexDirection='column'
            alignItems='center'
            justifyContent='center'
          >
            <CircularProgress />
            <Box mt={1}>
              <Typography>Creating resource...</Typography>
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    )
  }

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle id='form-create'>Create Container</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <DialogContentText>
              Enter the name of the container that you want to create. Later you can setup the ACL.
          </DialogContentText>
          <TextField fullWidth id='name-field' label='Name' required value={state.name} onChange={handleChange('name')} />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>
            Cancel
          </Button>
          <Button type='submit' disabled={state.submitted} color='primary'>Create</Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}
