import React from 'react'

import {
  IconButton,
  Menu,
  MenuItem,
  Divider,
  ListItem,
  ListItemText,
  ListItemIcon,
  ListItemSecondaryAction
} from '@material-ui/core'
import {
  InsertDriveFile as FileIcon,
  Folder as FolderIcon,
  MoreVert as MoreVertIcon
} from '@material-ui/icons'
import { toLabel } from '@assemblage/utils'
import { SessionContext, RootContext, Link, fileClient } from '@assemblage/components'

import { formatDistance } from 'date-fns'
import { fileSize } from 'humanize-plus'

import { ConfirmationDialog, WaitBackdrop } from '../ui'
import ResourceDialog from './resource-display-dialog'
import { Resource } from './resource'
import { Acl } from './acl'

function stop (event) {
  if (event) {
    event.preventDefault()
    event.stopPropagation()
  }
}

// TODO refactor and simplify state management
function ResourceListItem (props) {
  const { subjectUrl, value } = props
  const { info, types, isContainer } = value

  const { session } = React.useContext(SessionContext)
  const { navigateTo, reload } = React.useContext(RootContext)
  const [menuAnchor, setMenuAnchor] = React.useState(null)
  const [openDialog, setOpenDialog] = React.useState({
    openResource: false,
    openControl: false
  })
  const [confirmDialogState, setConfirmDialogState] = React.useState({
    open: false
  })
  const [waitingState, setWaitingState] = React.useState({
    open: false
  })

  const openMenu = event => {
    stop(event)
    setMenuAnchor(event.currentTarget)
  }

  const closeMenu = event => {
    stop(event)
    setMenuAnchor(null)
  }

  const openResourceDialog = event => {
    stop(event)
    closeMenu(event)
    setOpenDialog({
      openResource: true
    })
  }

  const closeResourceDialog = event => {
    stop(event)
    setOpenDialog({
      openResource: false
    })
  }

  const handleDelete = isContainer => event => {
    stop(event)
    closeMenu(event)
    setConfirmDialogState({
      open: true,
      title: `Do you want to delete '${toLabel(subjectUrl)}'?`,
      text: isContainer
        ? 'All contained workspaces will be deleted as well.'
        : 'Access control and metadata will be deleted as well.',
      onOk: async (event) => {
        closeConfirmDialog(event)
        setWaitingState({
          open: true,
          text: 'Deleting resource...'
        })
        try {
          if (isContainer) {
            await fileClient.deleteFolder(subjectUrl)
          } else {
            await fileClient.deleteFile(subjectUrl)
          }
          setWaitingState({
            open: false
          })
          // TODO get current view id
          reload('browser')
        } catch (error) {
          setWaitingState({
            open: false
          })
          // TODO show toast message
          console.error(error)
        }
      }
    })
  }

  const closeConfirmDialog = event => {
    stop(event)
    setConfirmDialogState({ open: false })
  }

  const openControlDialog = event => {
    stop(event)
    closeMenu(event)
    setOpenDialog({
      openControl: true
    })
  }

  const closeControlDialog = event => {
    stop(event)
    setOpenDialog({
      openControl: false
    })
  }

  const ResourceMenu = ({ isContainer }) => (
    <Menu
      id='resource-menu'
      anchorEl={menuAnchor}
      keepMounted
      open={Boolean(menuAnchor)}
      onClose={closeMenu}
    >
      <MenuItem onClick={openResourceDialog}>Details</MenuItem>
      {session.isOwner && [
        <MenuItem key='settings' onClick={openControlDialog}>Settings</MenuItem>,
        <Divider key='divider' />,
        <MenuItem key='delete' onClick={handleDelete(isContainer)}>
        Delete
        </MenuItem>
      ]}
    </Menu>
  )

  let secondaryText = formatDistance(new Date(info.modified), new Date())
  if (!isContainer) {
    secondaryText += ' – ' + fileSize(info.size)
  }

  return (
    <>
      <ListItem
        button onClick={event => {
          event.preventDefault()
          event.stopPropagation()

          navigateTo({
            url: subjectUrl
          })
        }}
      >
        <ResourceMenu isContainer={isContainer} />
        <ListItemIcon>{isContainer ? <FolderIcon /> : <FileIcon />}</ListItemIcon>
        <ListItemText
          primary={<Link href={subjectUrl}>{toLabel(subjectUrl)}</Link>}
          secondary={secondaryText}
        />
        <ListItemSecondaryAction>
          <IconButton
            edge='end'
            aria-controls='resource-menu'
            aria-haspopup='true'
            onClick={openMenu}
          >
            <MoreVertIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      {waitingState.open &&
        <WaitBackdrop {...waitingState} />}
      {openDialog.openResource &&
        <ResourceDialog
          open={openDialog.openResource}
          onClose={closeResourceDialog}
        >
          <Resource
            it={{ types, info }}
            {...props}
          />
        </ResourceDialog>}
      {openDialog.openControl &&
        <ResourceDialog
          open={openDialog.openControl}
          onClose={closeControlDialog}
        >
          <Acl
            {...props}
          />
        </ResourceDialog>}
      {confirmDialogState.open &&
        <ConfirmationDialog
          {...confirmDialogState}
          onCancel={closeConfirmDialog}
        />}
    </>
  )
}

export default ResourceListItem
