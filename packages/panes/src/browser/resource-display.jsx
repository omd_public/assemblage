import React from 'react'

import Catalog from './resource-catalog'

export default function ResourceDisplay ({ subjectUrl, types, ...props }) {
  const { display } = Catalog.getDisplaysFor(types)[0]
  const Display = display

  return <Display subjectUrl={subjectUrl} {...props} />
}
