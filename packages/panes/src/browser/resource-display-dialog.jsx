import React from 'react'
import {
  Container,
  Typography,
  IconButton,
  Dialog,
  Slide,
  AppBar,
  Toolbar
} from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'

const Transition = React.forwardRef(function Transition (props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

function ResourceDialog ({ subjectUrl, title, onClose, children, ...props }) {
  return (
    <Dialog fullScreen TransitionComponent={Transition} onClose={onClose} {...props}>
      <AppBar style={{ position: 'relative' }}>
        <Toolbar>
          <IconButton edge='start' color='inherit' onClick={onClose} aria-label='close'>
            <CloseIcon />
          </IconButton>
          {title &&
            <Typography variant='h6'>
              {title}
            </Typography>}
        </Toolbar>
      </AppBar>
      <Container maxWidth='lg'>
        {children}
      </Container>
    </Dialog>
  )
}

export default ResourceDialog
