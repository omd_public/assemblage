import React from 'react'

import {
  Dialog,
  DialogTitle as MuiDialogTitle,
  DialogContent as MuiDialogContent,
  Typography,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
  withStyles
} from '@material-ui/core'

import {
  Close as CloseIcon
} from '@material-ui/icons'

import { WaitBackdrop } from '../ui'
import Catalog from './resource-catalog'

const styles = theme => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  root: {
    margin: 0,
    padding: theme.spacing(2)
  }
})

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant='h6'>{children}</Typography>
      {onClose ? (
        <IconButton aria-label='close' className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent)

export default function CreateDialog (props) {
  const { open, onClose } = props
  const [state, setState] = React.useState({
    submitted: false
  })

  const handleSubmit = event => {
    event && event.preventDefault()

    if (!state.submitted) {
      setState({ ...state, submitted: true })
    }
  }

  const openCreator = entry => event => {
    event.preventDefault()
    event.stopPropagation()

    setState({
      ...state,
      label: entry.label,
      create: entry.create
    })
  }

  if (state.submitted) {
    return <WaitBackdrop open text='Creating resource...' />
  }

  if (state.create) {
    const Create = state.create
    return <Create beforeCreate={handleSubmit} {...props} />
  }

  const components = Catalog.getAllCreators()

  return (
    <Dialog open={open} onClose={onClose} aria-labelledby='add-resource-dialog-title'>
      <DialogTitle id='add-resource-dialog-title' onClose={onClose}>Add Resource</DialogTitle>
      <DialogContent dividers>
        <Typography>Choose the resource type to be added</Typography>
        <List>
          {components.map(entry => {
            return (
              <ListItem
                key={entry.label}
                button
                onClick={openCreator(entry)}
              >
                <ListItemAvatar>
                  <Avatar>{entry.icon}</Avatar>
                </ListItemAvatar>
                <ListItemText primary={entry.label} />
              </ListItem>
            )
          })}
        </List>
      </DialogContent>
    </Dialog>
  )
}
