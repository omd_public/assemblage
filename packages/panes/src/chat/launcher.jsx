import React from 'react'

import { LaunchButton } from '@assemblage/components'

import icon from './images/icon.svg'

export default function (props) {
  const [state, setState] = React.useState({
    open: false
  })

  return (
    <>
      <LaunchButton
        label='Chat'
        onClick={() => setState({ open: true })}
        src={icon}
      />
    </>
  )
}
