import React from 'react'

import { useReify } from '@assemblage/hooks'
import { accountInfo, expand } from '@assemblage/ld'

import Profile from '../profile/profile'
import Types from './types'

export default function ({ subjectUrl, store }) {
  const account = store.any(undefined, store.sym(expand('solid:account')))

  const state = useReify({
    acc: accountInfo
  }, {
    subjectUrl: account.value
  })

  // TODO: handle withLoading() :D
  if (state.loading) {
    return null
  }

  if (state.error) {
    throw new Error(state.error)
  }

  const entity = state.it
  const { acc } = entity

  return (
    <Profile store={entity.store} subjectUrl={account.value}>
      {acc &&
        <Types
          publicTypes={acc.publicTypes}
          privateTypes={acc.privateTypes}
          subjectUrl={subjectUrl}
        />}
    </Profile>
  )
}
