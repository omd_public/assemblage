import React from 'react'

import {
  Avatar,
  ListItem,
  ListItemAvatar,
  ListItemText,
  List
} from '@material-ui/core'
import { Folder, Notes, Chat, Description } from '@material-ui/icons'
import { Link, SessionContext } from '@assemblage/components'
import { useReify } from '@assemblage/hooks'
import { expand } from '@assemblage/ld'
import { toPathLabel } from '@assemblage/utils'

import { PaneBox, ListItemSkeleton } from '../ui'

const TypesMap = {
  'http://www.w3.org/ns/ldp#BasicContainer': {
    icon: <Folder />
  },
  'http://www.w3.org/ns/pim/meeting#LongChat': {
    icon: <Chat />
  },
  'http://www.w3.org/ns/pim/pad#Notepad': {
    icon: <Notes />
  }
}

function indexedThings ({ store }) {
  const things = []
  const triples = store.match(undefined, store.sym(expand('solid:forClass')))

  for (let i = 0; i < triples.length; i++) {
    const triple = triples[i]
    const solidClass = triple.object.value
    const instance = store.any(
      store.sym(triple.subject.value),
      store.sym(expand('solid:instance'))
    )
    things.push({
      instance: instance.value,
      solidClass
    })
  }

  return things
}

function Thing ({ thing }) {
  const state = useReify({
    title: ['dc:title']
  }, {
    subjectUrl: thing.instance
  })

  if (state.loading) {
    return <ListItemSkeleton />
  }

  if (state.error) {
    return null
  }

  const { title } = state.it
  const entry = TypesMap[thing.solidClass]

  return (
    <Link href={thing.instance}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            {entry ? entry.icon : <Description />}
          </Avatar>
        </ListItemAvatar>
        <ListItemText>
          {title || toPathLabel(thing.instance)}
        </ListItemText>
      </ListItem>
    </Link>
  )
}

function ThingList ({ indexFile, title }) {
  const state = useReify({
    things: indexedThings
  }, {
    subjectUrl: indexFile
  })

  if (state.loading) {
    return null
  }

  if (state.error) {
    return null
  }

  const { things } = state.it

  if (things === undefined || things.length === 0) {
    return null
  }

  return (
    <PaneBox title={title}>
      <List>
        {things && [].concat(things).map((thing, i) => {
          return (
            <Thing key={i} thing={thing} />
          )
        })}
      </List>
    </PaneBox>
  )
}

export default function ({ publicTypes, privateTypes }) {
  const { session } = React.useContext(SessionContext)

  return (
    <>
      {session.isOwner &&
        <ThingList title='Private Things' indexFile={privateTypes} />}
      <ThingList title='Public Things' indexFile={publicTypes} />
    </>
  )
}
