/**
 * generates:
 *  - dist/main.js
 *  - dist/manifest.json
 *  - dist/webpack-bundle-analyzer-report.html
 */
const webpack = require('webpack')
const WebpackAssetsManifest = require('webpack-assets-manifest')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

module.exports = {
  plugins: [
    new webpack.EnvironmentPlugin({
      'process.env.NODE_ENV': process.env.NODE_ENV
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false,
      reportFilename: 'webpack-bundle-analyzer-report.html'
    }),
    new WebpackAssetsManifest()
  ],
  entry: {
    account: './src/account/account.jsx',
    browser: './src/browser/browser.jsx',
    'browser-launcher': './src/browser/launcher.jsx',
    resource: './src/browser/resource.jsx',
    acl: './src/browser/acl/acl.jsx',
    profile: './src/profile/profile.jsx',
    preferences: './src/preferences/preferences.jsx',
    notepad: './src/notepad/notepad.jsx',
    'notepad-launcher': './src/notepad/launcher.jsx',
    'notepad-creator': './src/notepad/creator.jsx',
    'notepad-icon': './src/notepad/icon.jsx',
    chat: './src/chat/chat.jsx',
    'chat-launcher': './src/chat/launcher.jsx'
  },
  output: {
    libraryTarget: 'commonjs'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  externals: {
    'prop-types': 'prop-types',
    react: 'react',
    'solid-auth-client': 'solid-auth-client',
    'solid-file-client': 'solid-file-client',
    'styled-components': 'styled-components',
    rdflib: 'rdflib',
    '@assemblage/registry': '@assemblage/registry',
    '@assemblage/hooks': '@assemblage/hooks',
    '@assemblage/components': '@assemblage/components',
    '@assemblage/ld': '@assemblage/ld',
    '@assemblage/utils': '@assemblage/utils',
    '@assemblage/location': '@assemblage/location',
    'throttle-debounce': 'throttle-debounce',
    he: 'he',
    'humanize-plus': 'humanize-plus',
    'date-fns': 'date-fns',
    '@material-ui/core': '@material-ui/core',
    '@material-ui/lab': '@material-ui/lab',
    '@material-ui/core/styles': '@material-ui/core/styles',
    '@material-ui/pickers': '@material-ui/pickers',
    '@material-ui/icons': '@material-ui/icons',
    // Dependency required for material-ui date picker
    '@date-io/date-fns': '@date-io/date-fns',
    // File uploader
    'material-ui-dropzone': 'material-ui-dropzone',
    // Codemirror
    'react-codemirror': 'react-codemirror',
    'codemirror/config': 'codemirror/config'
  },
  module: {
    rules: [
      {
        test: /\.m?jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'url-loader'
        ]
      }
    ]
  },
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
    }
    // http2: true
  }
}
