/**
 * remote-component.config.js
 *
 * Dependencies for Remote Components
 */

module.exports = {
  resolve: {
    // Optimize imports here
    react: require('react'),
    rdflib: require('rdflib'),
    'solid-auth-client': require('solid-auth-client'),
    'solid-file-client': require('solid-file-client'),
    he: require('he'),
    'throttle-debounce': require('throttle-debounce'),
    'humanize-plus': require('humanize-plus'),
    'date-fns': require('date-fns'),
    '@assemblage/registry': require('./registry'),
    '@assemblage/hooks': require('./hooks'),
    '@assemblage/components': require('./components'),
    '@assemblage/ld': require('./ld'),
    '@assemblage/utils': require('./utils'),
    '@assemblage/location': require('./location'),
    // Material UI
    '@material-ui/core': require('./exports/material-ui'),
    '@material-ui/lab': require('./exports/material-ui-lab'),
    '@material-ui/core/styles': require('./exports/material-ui-styles'),
    '@material-ui/pickers': require('@material-ui/pickers'),
    '@material-ui/icons': require('./exports/material-ui-icons'),
    // Dependency required for material-ui date picker
    '@date-io/date-fns': require('@date-io/date-fns'),
    // File uploader
    'material-ui-dropzone': require('material-ui-dropzone'),
    'react-codemirror': require('react-codemirror2'),
    'codemirror/config': {
      base: require('codemirror/lib/codemirror.css'),
      material: require('codemirror/theme/material.css'),
      activeLine: require('codemirror/addon/selection/active-line.js'),
      brackets: require('codemirror/addon/edit/matchbrackets.js'),
      turtle: require('codemirror/mode/turtle/turtle.js'),
      html: require('codemirror/mode/htmlmixed/htmlmixed.js'),
      xml: require('codemirror/mode/xml/xml.js'),
      js: require('codemirror/mode/javascript/javascript.js')
    }
  }
}
