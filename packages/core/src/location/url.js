
import parseUrl from 'url-parse'

// TODO test that works without proxy!
// set up a POD with this browser :D
const proxyMode = process.env.ASSEMBLAGE_PROXY || true

function asUrl (str) {
  const decodedUrl = decodeURIComponent(str)
  const proxyUrl = parseUrl(decodedUrl)
  const pathname = proxyUrl.pathname ? proxyUrl.pathname : '/'
  return `${proxyUrl.protocol}//${proxyUrl.host}${pathname}${proxyUrl.query}${proxyUrl.hash}`
}

function asLocationObject (str) {
  const location = {}
  const view = extractView(str)
  if (view !== null) {
    location.view = view
    location.url = asUrl(str.replace(`${view}|`, ''))
  } else {
    location.url = asUrl(str)
  }
  return location
}

export function asFolderPath (location) {
  const parsed = parseUrl(location)
  const { protocol, host, pathname } = parsed
  let path
  if (pathname.endsWith('/')) {
    path = pathname
  } else {
    path = pathname.substr(0, pathname.lastIndexOf('/') + 1)
  }
  return `${protocol}//${host}${path}`
}

export function toAbsoluteUrl (subject, location) {
  return `${extractBase(subject)}${location}`
}

export function extractBase (location) {
  const parsed = parseUrl(location)
  const { protocol, host } = parsed
  return `${protocol}//${host}`
}

export function extractPathLinks (location, rootName = 'Root') {
  const base = extractBase(location)
  const here = location.split('/').slice(3)
  const parts = [{ text: rootName, href: base + '/' }]

  for (let i = 0; i < here.length; i++) {
    const part = here[i]
    const href = base + '/' + here.slice(0, i + 1).join('/')
    parts.push({ text: part, href: href + '/' })
  }

  const plen = parts.length

  if (plen === 2 && parts[1].text === '') {
    return []
  }

  return parts.slice(0, parts[plen - 1].text === '' ? plen - 1 : plen)
}

export function extractView (href) {
  const parts = href.split('|')
  if (parts.length > 1) {
    return parts[0]
  } else {
    return null
  }
}

export function removeView (pathname) {
  const view = extractView(pathname)
  if (view) {
    return pathname.replace(`${view}|`, '')
  }
  return pathname
}

export function parseQuery (query) {
  return new Map(query.slice(1).split('&').map(kv => kv.split('=')))
}

export function toProxiedUrl (href) {
  if (proxyMode) {
    const currentUrl = parseUrl(document.location)
    return `${currentUrl.protocol}//${currentUrl.host}/${encodeURIComponent(href)}`
  }
  return href
}

export function fromProxiedUrl (location) {
  const locationObject = {
    url: location,
    view: null
  }

  if (proxyMode) {
    const { pathname } = parseUrl(location)
    if (decodeURIComponent(pathname).indexOf('//') < 0) {
      // XXX temporary for proxy home
      return {
        url: null
      }
    } else {
      return asLocationObject(pathname.substr(1))
    }
  }

  return locationObject
}
