import React from 'react'
import ReactDOM from 'react-dom'

import { Bootloader } from './boot'
import App from './app'

Bootloader.start().then(() => {
  ReactDOM.render(<App />, document.getElementById('root'))
})
