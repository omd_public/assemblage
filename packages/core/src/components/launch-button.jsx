import React from 'react'

import {
  Box,
  Typography,
  Button,
  makeStyles
} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  label: {
    marginTop: theme.spacing(0.5)
  },
  icon: {
    width: '48px',
    height: '48px'
  }
}))

export default function LaunchButton ({ label, onClick, src }) {
  const classes = useStyles()

  return (
    <Button onClick={onClick}>
      <Box
        display='flex'
        flexDirection='column'
        justifyContent='center'
        alignItems='center'
      >
        <img src={src} className={classes.icon} alt='' />
        <Typography className={classes.label}>{label}</Typography>
      </Box>
    </Button>
  )
}
