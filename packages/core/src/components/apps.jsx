import React from 'react'

import {
  Box,
  IconButton,
  Typography,
  Drawer,
  Divider
} from '@material-ui/core'
import AppsIcon from '@material-ui/icons/Apps'

import { Registry } from '../registry'

export default function Apps (props) {
  const [open, setOpen] = React.useState(false)

  const closeDrawer = () => {
    setOpen(false)
  }

  const AppsButton = () => (
    <IconButton onClick={() => setOpen(true)}>
      <AppsIcon />
    </IconButton>
  )

  const PanesBox = () => {
    const panes = Registry.getLauncherPanes()

    return (
      <Box my={2} display='flex' flexDirection='column'> {
        panes.map((p, i) => {
          const Launcher = p.launchComponent
          return (
            <Box key={i} mb={1} flexGrow={1} display='flex' justifyContent='center'>
              <Launcher onSuccess={closeDrawer} {...props} />
            </Box>)
        })
      }
      </Box>
    )
  }

  return (
    <>
      <AppsButton />
      <Drawer anchor='right' open={open} onClose={closeDrawer}>
        <Box display='flex' flexDirection='column' justifyContent='center'>
          <Box m={2}>
            <Typography variant='h5'>
            Your Apps
            </Typography>
          </Box>
          <Divider />
          {open && <PanesBox />}
        </Box>
      </Drawer>
    </>
  )
}
