import React from 'react'
import {
  Avatar,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
  Paper,
  Divider
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { makeStyles } from '@material-ui/core/styles'
import auth from 'solid-auth-client'
import { idps } from '../../data/idp-list.json'

const useStyles = makeStyles(theme => ({
  content: {
    margin: theme.spacing(2, 0, 0, 0)
  },
  paper: {
    padding: '2px 4px',
    margin: theme.spacing(1, 0, 2, 0),
    display: 'flex',
    alignItems: 'center'
  },
  logInButton: {
    padding: 1
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1)
  }
}))

function LogInWindow ({ open, setLoading, setDialogOpen, ...props }) {
  const classes = useStyles()
  const [idp, setIdp] = React.useState({ url: null, complete: false })

  React.useEffect(() => {
    async function login () {
      // On successful connection to IDP, solid-auth-client redirects browser to IDP log in page
      const login = await auth.login(idp.url, { callbackUri: window.location.href })
      // solid-auth-client doesn't throw an error on unsuccessful connection to IDP :(
      // Instead it returns a null object
      if (login === null) {
        console.log(`Error at login with IDP: ${idp.url}`)
        setLoading(false)
      }
    }

    if (idp.complete) {
      setLoading(true)
      login()
    }
  }, [idp])

  const handleClose = () => {
    setDialogOpen(false)
  }

  const handleInputChange = event => setIdp({ url: event.target.value, complete: false })

  const signalInputComplete = (event) => {
    event.preventDefault()
    const merged = Object.assign({}, idp, { complete: true })
    setIdp(merged)
  }

  const handleIdpButtonClick = (url) => setIdp({ url, complete: true })

  return (
    <Dialog
      open={open}
      onClose={handleClose}
    >
      <DialogTitle>
        <IconButton className={classes.closeButton} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <DialogContentText className={classes.content}>Please enter your WebID or the URL of your identity provider:</DialogContentText>
        <Paper component='form' onSubmit={signalInputComplete} className={classes.paper} elevation={0}>
          <TextField
            autoFocus
            label='e.g. https://myidentity.provider'
            type='url'
            fullWidth
            onChange={handleInputChange}
            required
          />
          <Button type='submit' className={classes.logInButton} color='primary' autoFocus>
            Log in
          </Button>
        </Paper>
        <Divider />
        <DialogContentText className={classes.content}>Or select one of the following identity providers</DialogContentText>
        <List>
          {idps.map(provider => (
            <ListItem button key={provider.title} onClick={() => handleIdpButtonClick(provider.url)}>
              <ListItemAvatar>
                <Avatar src={provider.icon} alt={provider.title} />
              </ListItemAvatar>
              <ListItemText primary={provider.title} secondary={provider.description} />
            </ListItem>
          ))}
        </List>
      </DialogContent>
    </Dialog>
  )
}

export default LogInWindow
