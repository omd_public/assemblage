import React from 'react'
import {
  Box,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Drawer
} from '@material-ui/core'
import MenuRoundedIcon from '@material-ui/icons/MenuRounded'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import LogInWindow from './log-in-window'
import Loading from './loading'

const useStyles = makeStyles(theme => ({
  drawerButton: {
    border: '1px solid',
    borderRadius: theme.spacing(0.5),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    justifyContent: 'center'
  },
  drawerButtonText: {
    'font-size': '0.875rem',
    'font-weight': 500,
    'text-transform': 'uppercase'
  }
}))

function LoggedOut (props) {
  const classes = useStyles()
  const [dialogOpen, setDialogOpen] = React.useState(false)
  const [drawerOpen, setDrawerOpen] = React.useState(false)
  const [loading, setLoading] = React.useState(false)

  const theme = useTheme()
  const desktopMode = useMediaQuery(theme.breakpoints.up('sm'))

  if (loading) {
    return <Loading />
  }

  return desktopMode
    ? (
      <span {...props}>
        <Button variant='contained' color='primary' onClick={() => setDialogOpen(true)}>
      Log in
        </Button>
        <LogInWindow open={dialogOpen} setDialogOpen={setDialogOpen} setLoading={setLoading} {...props} />
      </span>
    )
    : (
      <span {...props}>
        <IconButton onClick={() => { setDrawerOpen(true) }}>
          <MenuRoundedIcon />
        </IconButton>
        <Drawer
          anchor='right'
          open={drawerOpen}
          onClose={() => { setDrawerOpen(false) }}
        >
          <Box p={2}>
            <List component='nav'>
              <ListItem
                button
                classes={{ button: classes.drawerButton }}
                onClick={() => setDialogOpen(true)}
              >
                <ListItemText classes={{ primary: classes.drawerButtonText }} primary='Log in' />
              </ListItem>
              <ListItem
                button
                classes={{ button: classes.drawerButton }}
              >
                <ListItemText classes={{ primary: classes.drawerButtonText }} primary='Sign up' />
              </ListItem>
            </List>
          </Box>
        </Drawer>
        <LogInWindow open={dialogOpen} setDialogOpen={setDialogOpen} setLoading={setLoading} {...props} />
      </span>
    )
}

export default LoggedOut
