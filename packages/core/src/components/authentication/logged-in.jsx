import React, { useState, useContext } from 'react'
import {
  Button,
  Avatar,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  Box,
  Divider
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import InboxRoundedIcon from '@material-ui/icons/InboxRounded'
import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded'
import StorageRoundedIcon from '@material-ui/icons/StorageRounded'

import auth from 'solid-auth-client'
import { SessionContext } from '../assemblage'
import Link from '../link'
import { single } from '../../ld'
import Loading from './loading'

const useStyles = makeStyles(theme => ({
  button: {
    'margin-bottom': theme.spacing(2)
  },
  account: {
    '&:hover': {
      'background-color': 'rgba(0, 0, 0, 0.04)'
    },
    'margin-top': theme.spacing(1),
    'margin-bottom': theme.spacing(1),
    'padding-top': theme.spacing(1),
    'padding-bottom': theme.spacing(1)
  },
  accountInfo: {
    'padding-top': theme.spacing(1)
  },
  avatarSmall: {
    width: theme.spacing(5),
    height: theme.spacing(5)
  },
  avatarLarge: {
    width: theme.spacing(10),
    height: theme.spacing(10)
  }
}))

function LoggedIn (props) {
  const { session, setSession } = useContext(SessionContext)
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const [loading, setLoading] = React.useState(false)
  const profileImage = single(session.account.img)

  const handleMenu = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const logout = async () => {
    setLoading(true)
    await auth.logout()
    setOpen(false)
    setLoading(false)
    setSession({ currentSession: null, isLoggedIn: false })
  }

  if (loading) {
    return <Loading />
  }

  return (
    <>
      <IconButton onClick={handleMenu}>
        <Avatar alt={session.account.name} src={profileImage} className={classes.avatarSmall} />
      </IconButton>
      <Drawer
        anchor='right'
        open={open}
        onClose={handleClose}
      >
        <Box p={2}>
          <Link underline='none' color='inherit' href={session.currentSession.webId} callback={handleClose}>
            <Box className={classes.account} display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
              <Avatar
                alt={session.account.name}
                src={profileImage}
                className={classes.avatarLarge}
              />
              <Typography className={classes.accountInfo} component='div'>
                <Box textAlign='center'>{session.account.name}</Box>
                <Box textAlign='center' fontStyle='oblique' fontSize={12}>{session.account.mBox.replace('mailto:', '')}</Box>
              </Typography>
            </Box>
          </Link>
          <Button
            className={classes.button}
            fullWidth
            variant='outlined'
          >
            <Link underline='none' color='inherit' href={session.account.preferences} callback={handleClose}>
              Manage your account
            </Link>
          </Button>
          <Divider />
          <List component='nav'>
            <Link underline='none' color='inherit' href={session.account.inbox} callback={handleClose}>
              <ListItem button>
                <ListItemIcon>
                  <InboxRoundedIcon />
                </ListItemIcon>
                <ListItemText primary='Inbox' />
              </ListItem>
            </Link>
            <Link underline='none' color='inherit' href={session.account.storage} view='browser' callback={handleClose}>
              <ListItem button>
                <ListItemIcon>
                  <StorageRoundedIcon />
                </ListItemIcon>
                <ListItemText primary='Storage' />
              </ListItem>
            </Link>
            <ListItem button onClick={logout}>
              <ListItemIcon>
                <ExitToAppRoundedIcon />
              </ListItemIcon>
              <ListItemText primary='Log out' />
            </ListItem>
          </List>
        </Box>
      </Drawer>
    </>
  )
}

export default LoggedIn
