import React from 'react'
import {
  CircularProgress,
  Dialog
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  progress: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
    overflow: 'hidden'
  },
  progressWheel: {
    color: 'white'
  }
}))

function Loading (props) {
  const classes = useStyles()
  return (
    <Dialog
      PaperProps={{
        classes: {
          root: classes.progress
        }
      }}
      open
      onClose={() => { }}
    >
      <CircularProgress className={classes.progressWheel} size={68} />
    </Dialog>
  )
}

export default Loading
