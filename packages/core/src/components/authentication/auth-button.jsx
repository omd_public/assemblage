import React from 'react'

import { LoggedIn, LoggedOut, ErrorToast } from '../index'
import { SessionContext } from '../assemblage'

function AuthButton ({ props }) {
  const { session } = React.useContext(SessionContext)

  if (session.error) {
    return (
      <>
        <LoggedOut {...props} />
        <ErrorToast message={session.error} />
      </>
    )
  }

  return session.isLoggedIn
    ? <LoggedIn subjectUrl={session.currentSession.webId} {...props} />
    : <LoggedOut {...props} />
}

export default AuthButton
