import React from 'react'

import { Registry } from '../registry'
import { useAsync } from '../hooks'

import PaneSwitch from './pane-switch'
import ErrorMessage from './error-message'

/**
 * Resolves pane views from the registry.
 *
 * This function can resolve remote pane views from the registry
 * by pane identifier or by registered conditions.
 * Notes on the resolution logic:
 * - If a pane identier is specified takes precedence over the registered
 * data conditions.
 * - When a view identifier is present it will be used to select that view
 * from the the resolved pane views if possible.
 *
 * @param {String} id The pane identifier, could be 'undefined'
 * @param {String} subjectUrl The subject URL
 * @param {String} viewId The selected viewd identifier, could be 'undefined'
 * @param {Object} placeholder The placeholder to show when loading
 * @param {Object} props Other properties
 * @returns {PaneSwitch} a pane switch with all the resolved views
 */
async function resolveViewPane (id, subjectUrl, viewId, placeholder, props) {
  const entries = id === undefined
    ? await Registry.getPanesbyConditions(subjectUrl)
    : [].concat(Registry.getPanebyId(id))

  return (
    <PaneSwitch
      views={entries}
      subjectUrl={subjectUrl}
      placeholder={placeholder}
      viewId={viewId}
      {...props}
    />
  )
}

/**
 * A remote pane view.
 *
 * Uses the 'resolve' function to materialize itself.
 *
 * @see resolveViewPane
 */
function PaneView ({ id, subjectUrl, viewId, forceReload, placeholder, onError, ...props }) {
  if (id === undefined && subjectUrl === undefined) {
    throw new Error('Either "subjectUrl" or "id" should be provided')
  }

  const {
    loading,
    error,
    it
  } = useAsync(
    resolveViewPane,
    [id, subjectUrl, viewId, placeholder, props],
    [id, subjectUrl, viewId, forceReload]
  )

  if (loading) {
    return placeholder || null
  }

  if (error) {
    return onError || <ErrorMessage container error={error} />
  }

  // The remote component
  return it
}

export default PaneView
