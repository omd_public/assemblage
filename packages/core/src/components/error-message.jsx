import React from 'react'

import { Container, Box } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import AlertTitle from '@material-ui/lab/AlertTitle'

function ErrorMessage ({ error, container }) {
  let content = 'Unknown error'
  let title = 'Error'

  const { status, statusText, url } = error

  if (status) {
    title = `${status} ${statusText}`
    content = `While fetching: ${url || error.message}`
  } else {
    content = error.message || error
  }

  const errorBox = (
    <Box my={3}>
      <Alert severity='error'>
        <AlertTitle>{title}</AlertTitle>
        {content}
      </Alert>
    </Box>
  )

  return container ? <Container>{errorBox}</Container> : errorBox
}

export default ErrorMessage
