import React from 'react'

import { Menu, MenuItem, Button, Box, Container } from '@material-ui/core'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'

import { SessionContext } from '.'

function PaneSwitchMenu ({
  views, label, viewId, update
}) {
  const { session } = React.useContext(SessionContext)
  const [anchorEl, setAnchorEl] = React.useState(null)

  const handleClick = event => {
    event.preventDefault()
    event.stopPropagation()

    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const handleMenuItemClick = (event, id) => {
    event.preventDefault()
    event.stopPropagation()

    if (id !== viewId) {
      update(id)
      setAnchorEl(null)
    }
  }

  return (
    <Box mt={2} display='flex'>
      <Button
        style={{ marginLeft: 'auto' }}
        onClick={handleClick}
        aria-label='change view'
        aria-haspopup='true'
        endIcon={<ArrowDropDownIcon />}
      >
        {label}
      </Button>
      <Menu
        id='views-menu'
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {
          views.map(({ id, label, audience }) => (
            // XXX just for demo
            !audience || session.isLoggedIn
              ? 
              <MenuItem
                key={id}
                selected={id === viewId}
                onClick={event => handleMenuItemClick(event, id)}
              >
                {label || id}
              </MenuItem>
              : null
          ))
        }
      </Menu>
    </Box>
  )
}

function PaneSwitch ({ subjectUrl, viewId, views, ...props }) {
  const [currentViewId, setCurrentViewId] = React.useState(viewId)
  // TODO handle no views case?
  const currentEntry = views.find(v => v.id === currentViewId)

  if (!currentEntry) {
    // Change to the first view id
    setCurrentViewId(views[0].id)
    return null
  }

  const CurrentViewPane = currentEntry.viewComponent

  return (
    <Container maxWidth='lg'>
      <PaneSwitchMenu
        views={views}
        label={currentEntry.label || currentEntry.id}
        viewId={currentViewId}
        update={setCurrentViewId}
      />
      <Box mt={2}>
        <CurrentViewPane
          id={currentEntry.id}
          subjectUrl={subjectUrl}
          {...props}
        />
      </Box>
    </Container>
  )
}

export default PaneSwitch
