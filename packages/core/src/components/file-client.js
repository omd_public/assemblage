import auth from 'solid-auth-client'
import FileClient from 'solid-file-client'

export const fileClient = new FileClient(auth)

function withHeaders (response, bodyPromise) {
  const { headers } = response
  return bodyPromise.then(data => {
    return {
      headers,
      data
    }
  })
}

export async function readFile (url, request) {
  const res = await fileClient.get(url, request)
  const type = res.headers.get('content-type')
  if (type && type.match(/(image|audio|video)/)) { return withHeaders(res, res.blob()) }
  if (res.text) { return withHeaders(res, res.text()) }
  return res
}
