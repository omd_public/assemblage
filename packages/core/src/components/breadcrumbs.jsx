import React from 'react'

import { Breadcrumbs as MuiBreadcrumbs, Typography } from '@material-ui/core'

import Link from './link'
import { extractPathLinks } from '../location'

function Breadcrumbs ({ subjectUrl, view, ...props }) {
  const pathLinks = extractPathLinks(subjectUrl)
  const size = pathLinks.length
  const links = pathLinks.map((l, i) => {
    const part = decodeURIComponent(l.text)
      .replace(/^\w/, c => c.toUpperCase())
    const hashIndex = part.indexOf('#')
    const label = hashIndex > -1 ? part.substr(0, hashIndex) : part

    if (subjectUrl === l.href || i === size - 1) {
      return <Typography key={i} color='textPrimary'>{label}</Typography>
    }

    return (
      <Link key={i} color='inherit' href={l.href} view={view}>
        {label}
      </Link>
    )
  })

  return (
    <MuiBreadcrumbs {...props}>
      {links}
    </MuiBreadcrumbs>
  )
}

export default Breadcrumbs
