import React from 'react'

import { CircularProgress } from '@material-ui/core'

import ErrorMessage from './error-message'

function withLoader (Component) {
  return function withReificationComponent ({
    state, placeholder, onError, ...props
  }) {
    const { it, loading, error } = state

    if (loading) {
      return placeholder || <CircularProgress />
    }

    if (error) {
      return onError || <ErrorMessage error={error} />
    }

    return <Component it={it} onError={onError} {...props} />
  }
}

export default withLoader
