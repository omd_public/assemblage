import React from 'react'

import {
  Box,
  Typography,
  TextField,
  makeStyles,
  CssBaseline,
  IconButton
} from '@material-ui/core'
import ArrowForwardIcon from '@material-ui/icons/ArrowForwardSharp'
import Autocomplete from '@material-ui/lab/Autocomplete'

import { toProxiedUrl } from '../location'

const useStyles = makeStyles(theme => ({
  hero: {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.35),rgba(0, 0, 0, 0.45)), url('/images/hero.jpg');",
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed'
  },
  textField: {
    '& .MuiInputBase-root': {
      backgroundColor: 'rgba(255, 255, 255, 0.7)'
    }
  },
  title: {
    color: 'white',
    fontSize: '3.5rem'
  },
  subTitle: {
    color: 'white',
    fontSize: '1.5rem',
    marginBottom: theme.spacing(4)
  },
  submitBtn: {
    width: '100%',
    marginTop: theme.spacing(0.5),
    color: 'white',
    backgroundColor: 'rgba(0,0,0, 0.15)',
    borderRadius: '4px'
  },
  [theme.breakpoints.up('sm')]: {
    title: {
      fontSize: '5rem'
    },
    subTitle: {
      fontSize: '2.5rem'
    },
    submitBtn: {
      width: 'auto',
      marginLeft: theme.spacing(0.5),
      marginTop: 0
    }
  }
}))

const webIDs = [
  'https://lilypomelo.inrupt.net',
  'https://bblfish.net/people/henry/card#me',
  'https://www.w3.org/People/Berners-Lee/card#i',
  'https://ruben.verborgh.org/profile/#me'
]

/**
 * Homepage for proxy mode Assemblage browser.
 * Includes example links for autocompletion.
 *
 * @param {*} props
 */
function ProxyHome (props) {
  const classes = useStyles()
  const [state, setState] = React.useState({})

  const handleSubmit = (event) => {
    event.preventDefault()

    const { url } = state
    if (url && url.indexOf('://') > -1) {
      window.location.href = toProxiedUrl(url)
    }
  }

  const handleChange = (event, value) => {
    setState({ url: value })
  }

  return (
    <div className={classes.hero}>
      <CssBaseline />
      <form onSubmit={handleSubmit}>
        <Box mt={5} px={1} py={1}>
          <Typography variant='h1' className={classes.title}>Welcome to Assemblage</Typography>
          <Typography variant='h4' className={classes.subTitle}>Your portal to the Web of Data</Typography>
          <Box display='flex' flexWrap='wrap'>
            <Box flexGrow={1}>
              <Autocomplete
                id='webid'
                freeSolo
                options={webIDs}
                inputValue={state.url}
                onInputChange={handleChange}
                renderInput={params => (
                  <TextField
                    {...params}
                    fullWidth
                    name='url'
                    variant='outlined'
                    placeholder='Enter a WebID'
                    className={classes.textField}
                  />
                )}
              />
            </Box>
            <IconButton
              type='submit'
              className={classes.submitBtn}
            >
              <ArrowForwardIcon />
            </IconButton>
          </Box>
        </Box>
      </form>
    </div>
  )
}

export default ProxyHome
