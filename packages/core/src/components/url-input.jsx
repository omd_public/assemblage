import React from 'react'

import {
  IconButton,
  Box,
  InputBase,
  InputAdornment,
  withStyles
} from '@material-ui/core'
import grey from '@material-ui/core/colors/grey'
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight'

import { RootContext } from './assemblage'

const StyledInput = withStyles((theme) => ({
  root: {
    padding: theme.spacing(0.5),
    paddingLeft: theme.spacing(2),
    borderRadius: '3px',
    backgroundColor: 'rgba(100,100,100,0.15)',
    color: grey[400],
    width: '100%',
    '&.Mui-focused': {
      color: 'black'
    },
    [theme.breakpoints.up('sm')]: {
      width: '15%',
      transition: 'width 0.5s ease',
      '&.Mui-focused': {
        width: '80%'
      }
    }
  }
}))(InputBase)

function SubjectUrlInput ({ location }) {
  const { navigateTo } = React.useContext(RootContext)
  const [state, setState] = React.useState({
    url: location.url
  })

  const handleSubmit = (e) => {
    e.preventDefault()
    navigateTo(state)
  }
  const handleChange = prop => event => {
    setState({ ...state, [prop]: event.target.value })
  }

  return (
    <form onSubmit={handleSubmit}>
      <Box display='flex'>
        <StyledInput
          required
          value={state.url}
          onChange={handleChange('url')}
          placeholder='Subject URL...'
          type='url'
          endAdornment={
            <InputAdornment position='end'>
              <IconButton type='submit' aria-label='search'>
                <KeyboardArrowRightIcon />
              </IconButton>
            </InputAdornment>
          }
        />
      </Box>
    </form>
  )
}

export default SubjectUrlInput
