import { toProxiedUrl } from '../location'

function createHistory () {
  const keyLength = 64
  const globalHistory = window.history
  // Are listeners needed?
  // Just a single navigate function
  // no need to clean up
  let navigateTo

  function listen () {
    window.addEventListener('popstate', function ({ state }) {
      if (state) {
        navigateTo({
          url: state.subject,
          view: state.view
        }, true)
      }
    })
  }

  function push (location) {
    const { url, view } = location
    const proxied = toProxiedUrl(url)
    globalHistory.pushState({
      subject: url,
      view,
      proxied
    }, createKey(), proxied)
  }

  function replace (location) {
    const { url, view } = location
    const proxied = toProxiedUrl(url)
    globalHistory.replaceState({
      subject: url,
      view,
      proxied
    }, createKey(), proxied)
  }

  function createKey () {
    return Math.random()
      .toString(36)
      .substr(2, keyLength)
  }

  function bindNavigateTo (nav) {
    navigateTo = nav
  }

  listen()

  return {
    push,
    replace,
    bindNavigateTo
  }
}

export default createHistory
