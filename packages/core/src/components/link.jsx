import React, { useContext } from 'react'
import { Link as MuiLink } from '@material-ui/core'

import { toProxiedUrl } from '../location'
import { RootContext } from './assemblage'

function Link ({ href, view, children, callback, ...props }) {
  const context = useContext(RootContext)

  const handleClick = (e, url) => {
    e.preventDefault()
    e.stopPropagation()
    e.nativeEvent.stopImmediatePropagation()

    if (callback) {
      callback()
    }

    context.navigateTo({
      url,
      view
    })
  }

  return (
    <MuiLink
      href={toProxiedUrl(href)}
      onClick={(e) => {
        handleClick(e, href)
      }} {...props}
    >
      {children}
    </MuiLink>
  )
}

export default Link
