import React from 'react'

import {
  AppBar,
  Toolbar,
  Container,
  makeStyles,
  Box
} from '@material-ui/core'
import grey from '@material-ui/core/colors/grey'

import PaneView from './pane-view'
import SubjectUrlInput from './url-input'
import createHistory from './history'
import Apps from './apps'
import AuthButton from './authentication/auth-button'
import useSession from '../hooks/session'

export const history = createHistory()
export const RootContext = React.createContext()
export const SessionContext = React.createContext()

const useStyles = makeStyles(theme => ({
  main: {
    flexGrow: 1
  },
  toolbar: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  appbar: {
    borderBottom: `1px solid ${grey[300]}`
  },
  apps: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  }
}))

/**
 * Assemblage is the outermost composition container.
 * Provides a navigational context and the initial pane resolution.
 */
function Assemblage ({ location, ...props }) {
  const classes = useStyles()

  const [currentLocation, setCurrentLocation] = React.useState(location)
  const [session, setSession] = useSession({ location: currentLocation, history })

  /**
   * Navigates to the specified location.
   *
   * This function is provided in the root context.
   *
   * @param {Object} newLocation The new location object.
   * @param {boolean} replace Indicates if the locations should be pushed or replaced in history.
   */
  const navigateTo = (newLocation, replace = false) => {
    setCurrentLocation(newLocation)

    if (replace) {
      history.replace(newLocation)
    } else {
      history.push(newLocation)
    }
  }

  /**
   * Reloads the current location with the view specified.
   *
   * This function is provided in the root context.
   *
   * @param {String} view The view identifier
   */
  const reload = (view) => {
    setCurrentLocation({
      url: currentLocation.url,
      forceReload: Math.random(),
      view
    })
  }

  history.bindNavigateTo(navigateTo)

  // Wait until the session is resolved
  if (session.loading) {
    return null
  }

  return (
    <RootContext.Provider value={{ currentLocation, navigateTo, reload }}>
      <SessionContext.Provider value={{ session, setSession }}>
        <AppBar
          position='static'
          color='transparent'
          elevation={0}
          className={classes.appbar}
        >
          <Container maxWidth='lg'>
            <Toolbar disableGutters className={classes.toolbar}>
              <Box
                display='flex'
                flexGrow={1}
                alignItems='center'
              >
                <Box flexGrow={1}>
                  <SubjectUrlInput location={currentLocation} />
                </Box>
                <Box ml='auto' className={classes.apps}>
                  {session.isLoggedIn && <Apps />}
                  <AuthButton />
                </Box>
              </Box>
            </Toolbar>
          </Container>
        </AppBar>
        <main className={classes.main}>
          <PaneView
            subjectUrl={currentLocation.url}
            viewId={currentLocation.view}
            forceReload={currentLocation.forceReload}
            {...props}
          />
        </main>
      </SessionContext.Provider>
    </RootContext.Provider>
  )
}

export default Assemblage
