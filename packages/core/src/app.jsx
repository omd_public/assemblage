import React from 'react'

import { CssBaseline, LinearProgress } from '@material-ui/core'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'

import { Assemblage, ProxyHome } from './components'
import { fromProxiedUrl } from './location'

function App () {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')

  const theme = React.useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: prefersDarkMode ? 'dark' : 'light'
        }
      }),
    [prefersDarkMode]
  )

  const location = fromProxiedUrl(document.location)

  if (location.url === null) {
    return <ProxyHome />
  }

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Assemblage
        location={location} placeholder={
          <LinearProgress />
        }
      />
    </ThemeProvider>
  )
}

export default App
