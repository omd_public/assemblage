import * as $rdf from 'rdflib'
import NS from 'solid-namespace'

const _ns = NS()

export function expand (text) {
  let expanded = text
  if (text.indexOf(':') > -1 &&
    text.indexOf('//') < 0) {
    const parts = text.split(':')
    expanded = _ns[parts[0]](parts[1])
  }
  return expanded
}

export function ns (text) {
  return $rdf.sym(expand(text))
}
