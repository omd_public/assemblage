import * as $rdf from 'rdflib'

import { expand } from './namespaces'

export function loadRDF (fetcher, url, currentDepth = 0, maxDepth = 1) {
  if (currentDepth > maxDepth) {
    Promise.resolve(true)
  }

  // Not sure if its a good idea to cache resources
  // to avoid the fetch to resolve the registry panes
  // + the fetch of the pane itself
  return new Promise((resolve, reject) => {
    fetcher.nowOrWhenFetched(url, function (ok, body, xhr) {
      if (ok) {
        // Load extended information in the graph
        const seeAlso = fetcher.store.each($rdf.sym(url), $rdf.sym(expand('rdfs:seeAlso')))
        if (seeAlso && seeAlso.length > 0) {
          const promises = []
          for (let i = 0; i < seeAlso.length; i++) {
            const extUrl = seeAlso[i]
            if (extUrl.termType === 'NamedNode') {
              promises.push(loadRDF(fetcher, extUrl.value, currentDepth + 1))
            }
            Promise.all(promises).then(resolve).catch(reject)
          }
        } else {
          resolve(ok)
        }
      } else {
        // TODO handle proper error
        reject(xhr)
      }
    })
  })
}

export function createFetcher (store) {
  return new $rdf.Fetcher(store, 5000)
}

export function createUpdater (store) {
  if (store === undefined) {
    throw new Error('Please provide a store to create an updater')
  }
  return new $rdf.UpdateManager(store, 5000)
}
