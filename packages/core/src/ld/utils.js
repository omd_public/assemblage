import * as $rdf from 'rdflib'

import { expand } from './namespaces'

export function asSym (obj) {
  return obj.termType ? obj : $rdf.sym(obj)
}

/**
 * @param {*} values
 */
export function toValues (values, locale = 'en') {
  if (values === undefined || values === null || !Array.isArray(values)) {
    return values
  }

  const filtered = values.filter(v => {
    if (v.termType === 'Literal' && v.lang) {
      return v.lang === locale
    }
    return true
  })

  if (filtered.length === 1) {
    return filtered.pop().value
  }
  if (filtered.length === 0) {
    return undefined
  }

  return filtered.map(v => v.value)
}

export function firstMatching (graph, subject, exprs) {
  for (let i = 0; i < exprs.length; i++) {
    const expr = exprs[i]
    const values = graph.each(subject, expr)
    if (values.length > 0) {
      return values
    }
  }
  return null
}

export function single (value) {
  if (Array.isArray(value)) {
    return value[0]
  }
  return value
}

export function anyObject (store, subject, predicate) {
  return store.any(asSym(subject), store.sym(expand(predicate)))
}

export function eachObject (store, subject, predicate) {
  return store.each(asSym(subject), store.sym(expand(predicate)))
}

export function anySubject (store, predicate, object) {
  return store.any(undefined, store.sym(expand(predicate)), asSym(object))
}
