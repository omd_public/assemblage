import * as $rdf from 'rdflib'
import { isEmptyObj } from '../utils'
import { toValues, firstMatching } from './utils'
import { expand } from './namespaces'

const infoSets = {
  basic: [
    { name: 'title', predicates: ['dc:title'] },
    { name: 'license', predicates: ['dc:license'] },
    { name: 'abstract', predicates: ['dc:abstract'] },
    { name: 'modified', predicates: ['dc:modified', 'dct:modified'] },
    { name: 'created', predicates: ['dc:created'] },
    { name: 'label', predicates: ['dc:label'] },
    { name: 'size', predicates: ['stat:size'] }
  ],
  acl: [
    {
      name: '*',
      predicates: ['acl:accessTo'],
      // XXX Note that this requires a matching object,
      // for the moment we assume that no different resources
      // will be mixed in the same acl doc.
      // object: 'acl:Authorization',
      subSet: [
        { name: 'agent', predicates: ['acl:agent'] },
        { name: 'agentGroup', predicates: ['acl:agentGroup'] },
        { name: 'agentClass', predicates: ['acl:agentClass'] },
        { name: 'origin', predicates: ['acl:origin'] },
        { name: 'originClass', predicates: ['acl:originClass'] },
        { name: 'accessTo', predicates: ['acl:accessTo'] },
        { name: 'default', predicates: ['acl:default'] },
        { name: 'defaultForNew', predicates: ['acl:defaultForNew'] },
        { name: 'modes', predicates: ['acl:mode'] }
      ]
    }
  ],
  account: [
    { name: 'name', predicates: ['vcard:fn', 'foaf:name'] },
    { name: 'img', predicates: ['foaf:img', 'vcard:hasPhoto'] },
    { name: 'nick', predicates: ['foaf:nick'] },
    { name: 'account', predicates: ['solid:account'] },
    { name: 'storage', predicates: ['space:storage'] },
    { name: 'inbox', predicates: ['ldp:inbox'] },
    { name: 'preferences', predicates: ['space:preferencesFile'] },
    { name: 'publicTypes', predicates: ['solid:publicTypeIndex'] },
    { name: 'privateTypes', predicates: ['solid:privateTypeIndex'] }
  ],
  profile: [
    { name: 'name', predicates: ['foaf:name', 'vcard:fn', 'rdfs:label'] },
    { name: 'gender', predicates: ['foaf:gender'] },
    { name: 'birthday', predicates: ['foaf:birthday'] },
    { name: 'familyName', predicates: ['foaf:familyName', 'foaf:lastName', 'foaf:family_name'] },
    { name: 'givenName', predicates: ['foaf:givenName', 'foaf:firstName', 'foaf:givenname'] },
    { name: 'title', predicates: ['foaf:title'] },
    { name: 'basedNear', predicates: ['foaf:based_near'] },
    { name: 'thumbnail', predicates: ['foaf:thumbnail'] },
    { name: 'photo', predicates: ['foaf:img', 'foaf:depiction', 'vcard:hasPhoto'] },
    { name: 'nick', predicates: ['foaf:nick'] },
    { name: 'logo', predicates: ['foaf:logo'] },
    { name: 'homepage', predicates: ['foaf:homepage'] },
    { name: 'weblog', predicates: ['foaf:weblog'] },
    { name: 'address', predicates: ['vcard:hasAddress'] },
    { name: 'mbox', predicates: ['foaf:mbox'] },
    { name: 'mboxSha1Sum', predicates: ['foaf:mbox_sha1sum'] },
    { name: 'email', predicates: ['vcard:hasEmail'] },
    { name: 'telephone', predicates: ['foaf:phone', 'vcard:hasTelephone'] },
    { name: 'openid', predicates: ['foaf:openid'] },
    { name: 'org', predicates: ['vcard:organization-name'] },
    { name: 'member', predicates: ['foaf:member'] },
    { name: 'role', predicates: ['vcard:role'] },
    { name: 'note', predicates: ['vcard:note'] },
    { name: 'account', predicates: ['foaf:account', 'foaf:holdsAccount'] },
    { name: 'status', predicates: ['foaf:status'] },
    { name: 'knows', predicates: ['foaf:knows'] },
    { name: 'trustedApps', predicates: ['acl:trustedApp'] }
  ]
}

function wildCardInfoSet ({ store, subjectUrl }, { object, predicates, subSet }) {
  // Infoset for all subjects
  // TODO add support for more :D
  const predicate = predicates[0]
  const selectorsBySubj = []
  const subjectUrls = store.each(
    undefined,
    $rdf.sym(expand(predicate))//,
    // $rdf.sym(expand(object))
  )
  for (let i = 0; i < subjectUrls.length; i++) {
    const s = subjectUrls[i]
    if (s.termType === 'NamedNode') {
      selectorsBySubj.push({
        name: s.value,
        subSet,
        predicates,
        subject: s.value
      })
    }
  }
  return infoSet({ store, subjectUrl }, selectorsBySubj)
}

export function infoSet ({ store, subjectUrl }, selectors) {
  const info = {}

  for (let i = 0; i < selectors.length; i++) {
    const { subject, name, predicates, subSet } = selectors[i]
    if (name === '*') {
      return wildCardInfoSet({ store, subjectUrl }, selectors[i])
    }
    if (subSet) {
      info[name] = infoSet({ store, subjectUrl }, subject ? subSet.map(entry => {
        return {
          subject,
          ...entry
        }
      }) : subSet)
    } else {
      const expanded = predicates.map(t => $rdf.sym(expand(t)))
      let effectiveSubj = subject || subjectUrl
      if (typeof effectiveSubj === 'string') {
        if (effectiveSubj.indexOf('://') > -1) {
          effectiveSubj = $rdf.sym(effectiveSubj)
        } else {
          effectiveSubj = $rdf.blankNode(effectiveSubj)
        }
      }
      const val = firstMatching(store, effectiveSubj, expanded)
      if (val) {
        info[name] = toValues(val)
      }
    }
  }

  return isEmptyObj(info) ? null : info
}

export function first ({ store, subjectUrl }, predicates) {
  const expanded = predicates.map(t => $rdf.sym(expand(t)))
  const val = firstMatching(store, $rdf.sym(subjectUrl), expanded)
  return toValues(val)
}

export function basicInfo (graphOpts) {
  return infoSet(graphOpts, infoSets.basic)
}

export function aclInfo (graphOpts) {
  return infoSet(graphOpts, infoSets.acl)
}

export function accountInfo (graphOpts) {
  return infoSet(graphOpts, infoSets.account)
}

export function profileInfo (graphOpts) {
  return infoSet(graphOpts, infoSets.profile)
}

export function rdfTypes ({ store, subjectUrl }) {
  const val = store.each($rdf.sym(subjectUrl), $rdf.sym(expand('rdf:type')))
  return toValues(val)
}

export function getPredicates (context, name) {
  const info = infoSets[context].find(item => item.name === name)
  return info.predicates
}
