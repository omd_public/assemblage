import * as $rdf from 'rdflib'

import { toValues } from './utils'
import { expand } from './namespaces'
import { createFetcher, loadRDF } from './rdf'

/**
 * Helper function to iterate on RDF objects.
 * @param {String} predicate RDF predicate
 * @param {Function} transformer The transformer function
 */
export function each (predicate, transformer) {
  return ({ store, subjectUrl }) => {
    const expanded = $rdf.sym(expand(predicate))
    // XXX if no final slash is passed when 'needed' the returned values are broken
    // to reproduce try ldp:contains in 'profile' instead of 'profile/'
    const vos = store.each($rdf.sym(subjectUrl), expanded, undefined)
    const values = toValues(vos)

    if (values) {
      return [].concat(values).map(value => {
        return {
          subjectUrl: value,
          value: transformer({ store, subjectUrl: $rdf.sym(value) })
        }
      })
    } else {
      return []
    }
  }
}

export async function load (subject) {
  const subjToLoad = $rdf.sym(subject)
  const store = $rdf.graph()
  const fetcher = createFetcher(store)
  await loadRDF(fetcher, subjToLoad)
  return { store, fetcher }
}
