import React from 'react'
import * as $rdf from 'rdflib'

import { Conditions } from '.'
import { loadRDF, createFetcher, createUpdater } from '../ld/rdf'

const entries = []

/**
 * The pane registry.
 */
class Registry {
  static register (entry) {
    entries.push(entry)
  }

  /**
   *
   */
  static getLauncherPanes () {
    return entries.filter(entry => entry.launchComponent !== undefined)
  }

  /**
   *
   */
  static getCreatorPanes (app) {
    return entries.filter(entry => entry.creatorComponent !== undefined &&
      entry.creator.allowedIn.indexOf(app) > -1)
  }

  /**
   *
   * @param {*} subjectUrl
   * @param {*} opts
   */
  static async getPanesbyConditions (subjectUrl, opts = {}) {
    // console.debug('Trying to resolve', subjectUrl, opts)

    const found = []
    const store = opts.store || $rdf.graph()
    const fetcher = opts.fetcher || createFetcher(store)
    const updater = opts.updater || createUpdater(store)

    const docSubject = subjectUrl.indexOf('#') > -1
      ? subjectUrl.split('#')[0]
      : subjectUrl

    await loadRDF(fetcher, docSubject)

    for (let i = 0; i < entries.length; i++) {
      const {
        id,
        label,
        conditions,
        audience,
        viewComponent,
        launchComponent
      } = entries[i]

      const statement = Conditions.match(store, subjectUrl, conditions)

      if (statement !== null) {
        const matchingSubject = statement.subject.value
        const Component = viewComponent

        found.push({
          id,
          conditions,
          label,
          audience,
          launchComponent,
          viewComponent: function ResolvedPane (props) {
            return (
              <Component
                {...props}
                // overrides
                id={id}
                store={store}
                fetcher={fetcher}
                updater={updater}
                subjectUrl={matchingSubject}
              />)
          }
        })
      }
    }

    if (found.length === 0) {
      throw new Error('Pane not found for current graph')
    }

    console.debug('Found in registry for', docSubject, 'panes', found)

    return found
  }

  /**
   *
   * @param {String} id
   */
  static getPanebyId (id) {
    const found = entries.find(entry => entry.id === id)
    if (found === undefined) {
      throw new Error(`Pane not found for id ${id}`)
    }
    return found
  }
}

export default Registry
