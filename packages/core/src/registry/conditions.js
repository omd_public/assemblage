import * as $rdf from 'rdflib'

import { expand } from '../ld'

function getMatchingStatement (graph, subject, predicate, toMatch) {
  // console.debug('Matching', subject, predicate, 'to', toMatch)

  const statements = graph.statementsMatching(subject, predicate, undefined)

  for (let i = 0; i < statements.length; i++) {
    const statement = statements[i]
    // console.debug('- type', statement.object)

    // Just checks the presence of the predicate
    if (toMatch.includes('*')) {
      return statement
    }

    // Matches object values
    if (toMatch.some(match => (statement.object.value === match))) {
      return statement
    }
  }

  return null
}

class Conditions {
  static match (graph, subjectUrl, conditions) {
    if (conditions === undefined) {
      return null
    }

    // TODO encapsulate and refactor, predicate logic
    const { predicates, strict } = conditions

    if (predicates === undefined) {
      return null
    }

    for (let i = 0; i < predicates.length; i++) {
      // TODO do not assume just first
      const [predicate, matching] = Object.entries(predicates[i])[0]
      const subject = matching.subjectUrl === undefined
        ? strict ? subjectUrl : undefined : undefined
      const values = matching.$any

      if (values !== undefined) {
        const expandedValues = values.map(v => v === '*' ? v : expand(v))
        const s = subject === undefined ? undefined
          : subject === '*' ? undefined : $rdf.sym(subject)
        const statement = getMatchingStatement(graph, s, $rdf.sym(expand(predicate)), expandedValues)
        if (statement !== null) {
          return statement
        }
      }
    }

    return null
  }
}

export default Conditions
