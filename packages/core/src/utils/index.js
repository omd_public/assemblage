export function appendPath (url, toAppend) {
  const base = url.charAt(url.length - 1) === '/'
    ? url : url + '/'
  const toAppendName = toAppend.charAt(0) === '/'
    ? toAppend.substr(1) : toAppend
  return `${base}${toAppendName}`
}

export function toLabel (subjectUrl) {
  if (!subjectUrl) {
    return subjectUrl
  }
  const { pathname } = new URL(subjectUrl)
  const parts = pathname.split('/')
  let labelPart = parts[parts.length - 1]
  if (labelPart === '') {
    labelPart = parts[parts.length - 2]
  }
  return decodeURIComponent(labelPart)
}

export function toPathLabel (subjectUrl) {
  const { pathname } = new URL(subjectUrl)
  const parts = pathname.split('/')
  const labelPart = parts.slice(1).join('/')
  return decodeURIComponent(labelPart)
}

export function isEmptyObj (obj) {
  for (const x in obj) { return false }
  return true
}

export function uniq (a) {
  var seen = {}
  var out = []
  var len = a.length
  var j = 0
  for (var i = 0; i < len; i++) {
    var item = a[i]
    if (seen[item] !== 1) {
      seen[item] = 1
      out[j++] = item
    }
  }
  return out
}

export function slugify (str) {
  str = str.replace(/^\s+|\s+$/g, '') // trim
  str = str.toLowerCase()

  // remove accents, swap ñ for n, etc
  const from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;'
  const to = 'aaaaaeeeeeiiiiooooouuuunc------'
  for (let i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes

  return str
}
