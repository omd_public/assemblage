import { toLabel } from './index'

test('handles null values', () => {
  expect(toLabel(null)).toBe(null)
})

test('handles empty values', () => {
  expect(toLabel('')).toBe('')
})
