/*
 * src/components/RemoteComponent.js
 */

import {
  createRemoteComponent,
  createRequires
} from '@paciolan/remote-component'
import { resolve } from '../remote-component.config.js'

const requires = createRequires(resolve)
export default createRemoteComponent({ requires })
