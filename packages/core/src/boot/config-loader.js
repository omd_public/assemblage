/**
 * Loads the registry configuration
 */
// @ts-ignore
import jsonConfig from '../registry-config.json'

async function load () {
  return jsonConfig
}

export default {
  load
}
