// @ts-check
import React from 'react'

import RemoteComponent from './remote-component'

import Config from './config-loader'
import { Registry } from '../registry'

function componentFor (entry) {
  if (entry && entry.url) {
    return props => <RemoteComponent url={entry.url} {...props} />
  }
  return undefined
}

async function start () {
  const config = await Config.load()

  console.log('Registry bootstrap start')
  console.log(`* Config: ${JSON.stringify(config, null, 2)}`)

  for (let i = 0; i < config.panes.length; i++) {
    const entry = config.panes[i]
    const { launcher, creator, icon } = entry

    // TODO validate entry consistency
    const viewComponent = componentFor(entry)
    const launchComponent = componentFor(launcher)
    const creatorComponent = componentFor(creator)
    const iconComponent = componentFor(icon)

    Registry.register({
      viewComponent,
      launchComponent,
      creatorComponent,
      iconComponent,
      ...entry
    })

    console.log(`- Component registered [${entry.id}@v:${entry.url}]`)
  }

  console.log('Registry bootstrap end')
}

export default {
  start
}
