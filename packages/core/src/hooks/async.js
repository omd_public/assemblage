import { useState, useEffect } from 'react'

/**
 * Asynchronous execution hook.
 * Provides loading, error and result in a property named "it".
 *
 * @param {Function} fn The async function
 * @param {Array} args The array of function arguments
 * @param {Array} deps The array of dependencies
 */
function useAsync (fn, args, deps = []) {
  const [data, setData] = useState({
    loading: true
  })

  const effect = () => {
    let running = true

    setData({
      loading: true
    })

    async function exec (_args) {
      try {
        const it = await fn(..._args)
        if (running) {
          setData({
            loading: false,
            it
          })
        }
      } catch (err) {
        if (running) {
          setData({
            loading: false,
            error: err
          })
        }
      }
    }
    exec(args)
    return () => {
      running = false
    }
  }

  useEffect(effect, deps)

  return data
}

export default useAsync
