import useAsync from './async'

function update (updater, ins, del = []) {
  if (updater === undefined) {
    throw new Error('RDF Updater is required for update call')
  }
  if (ins === undefined) {
    throw new Error('Insert statement is required for update call')
  }
  return new Promise((resolve, reject) => {
    updater.update(del, ins, (uri, ok, message) => {
      if (ok) {
        resolve(ok)
      } else {
        reject(message)
      }
    })
  })
}

function useUpdate (opts, deps) {
  return useAsync(update, [opts], deps)
}

export { update, useUpdate }
