import * as $rdf from 'rdflib'

import useAsync from './async'
import { loadRDF, first, createFetcher, createUpdater } from '../ld'

/**
 * Constructs an entity of reified values
 * while resolving ld-flex expressions.
 *
 * @param {Object} fields The map of names and proxied paths
 */
async function reify (fields, opts) {
  const { subjectUrl } = opts
  let { store, fetcher, updater } = opts

  if (subjectUrl === undefined) {
    throw new Error('Please, provide a subject URL to load the graph store')
  }

  const entity = {}

  // If no store is given then
  // create one and load the subject
  if (store === undefined) {
    store = $rdf.graph()
    fetcher = createFetcher(store)
    updater = createUpdater(store)
    await loadRDF(fetcher, subjectUrl)
  }

  // Pass the store to upstream :P
  entity.store = store
  entity.updater = updater

  const fieldEntries = Object.entries(fields)

  for (let i = 0; i < fieldEntries.length; i++) {
    const [key, expr] = fieldEntries[i]

    if (typeof expr === 'function') {
      const values = await expr({
        store,
        subjectUrl,
        fetcher
      })
      entity[key] = values
    } else {
      entity[key] = first({
        store, subjectUrl
      }, [].concat(expr))
    }
  }

  return entity
}

function useReify (fields, opts) {
  return useAsync(reify, [fields, opts])
}

export { useReify, reify }
