import React from 'react'

import auth from 'solid-auth-client'

import { reify } from './reify'
import { accountInfo, infoSet } from '../ld'

export default function useSession (opts) {
  const [session, setSession] = React.useState({ loading: true, currentSession: null, isLoggedIn: false })

  const effect = () => {
    let mounted = true

    async function getCurrentSession ({
      location, history
    }) {
      try {
        let isOwner = false
        const currentSession = await auth.currentSession()
        // Replacement of window location in history needs to be done after retrieving current session
        // This is because solid-auth-client checks window.location for 'access_token' hash value after redirect from provider log in page
        history.replace(location)
        let account = null
        if (currentSession) {
          const acc = await reify({
            info: accountInfo
          }, {
            subjectUrl: currentSession.webId
          })
          // TODO temporary for demonstration
          isOwner = location.url.startsWith(acc.info.storage)
          const pref = await reify({
            info: (graphOpts) => {
              const selector = [
                { name: 'userType', predicates: ['rdf:type'], subject: currentSession.webId },
                { name: 'mBox', predicates: ['foaf:mbox'], subject: currentSession.webId }
              ]
              return infoSet(graphOpts, selector)
            }
          }, {
            subjectUrl: acc.info.preferences
          })
          account = Object.assign({}, acc.info, pref.info)
        }
        const isLoggedIn = !!currentSession
        if (mounted) {
          setSession({
            loaded: true,
            currentSession,
            isOwner,
            isLoggedIn,
            account
          })
        }
      } catch (err) {
        console.log('Error getting current session ', err)
        if (mounted) {
          setSession({ error: 'There\'s an error getting your session' })
        }
      }
    }

    getCurrentSession(opts)

    return () => {
      mounted = false
    }
  }

  React.useEffect(effect, [])

  return [session, setSession]
}
