# Overview

Assemblage is a composable data browser aimed to provide a great user experience for Solid POD services,
as well as other RDF sources, both for desktop and mobile web browsers.

The frontend uses a registry of remote web components that can be composed into views and applications in accordance to
conditions over the underlying RDF data. Each view or application could have a different output depending on access control
policies, ownership and roles, e.g. owner, reader, writer &c.

The main javascript bundle can be served from the POD service itself, like mashlib, or deployed
in a separate HTTP server as an static resource that will proxy requests based on the URL path.

You can see it in action in the following video: https://youtu.be/hYFEBAdkzmc

## Technology Readiness Level

Assemblage is at the _concept validation_ level, altough the implementation is 100% functional.

# Development

## Using Docker Compose
To start the development environment using [Docker Compose](https://docs.docker.com/compose/), in the project root:
```bash
docker-compose up
```

And you should find the app running at [http://127.0.0.1:3001](http://127.0.0.1:3001)

## Without Containers

### Dependencies

* [Node.js](https://nodejs.org/) >=10 LTS
* [Lerna](https://lerna.js.org/)

### Installation

In the project root:
```bash
npm install
```

Use lerna to bootstrap packages:
```bash
lerna bootstrap
```

### Running All Services
To run all the services defined in the `Procfile` you will need a process manager like [foreman](https://github.com/ddollar/foreman) or [overmind](https://github.com/DarthSim/overmind) installed in your system.

In the project root:
```bash
overmind s
```

### Running Panes Service
Inside `packages/panes`:
```bash
npm run start
```

### Running Browser Service
Inside `packages/core`:
```bash
npm run start
```
Notes:
* The browser uses the panes served in the `panes` package. If `panes` is not running, the browser will return an error.
* `React-scripts start` could take a while (~30 secs).

# License

This project is licensed under the GNU Affero General Public License v3.0.
